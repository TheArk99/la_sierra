#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

/*
Write a program to input a c-string, and then print out the same c-string but with all of the
letters capitalized.
*/

int main(){
  char *userString;
  int i = 0;
  cin.getline(userString, strlen(userString));
  while (userString[i] != '\0'){
    userString[i] = toupper(userString[i]);
    i++;
  }
  cout << userString << endl;
  return 0;
}
