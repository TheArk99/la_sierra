#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

/*
Another guessing game. Create a pattern string of random length from 5 up to 10
characters long, and initialize it with random digits, e.g. 3687073452. The user will then try
to guess the digits one at a time. The pattern string is hidden from the user, and the program
just prints out *'s to represent this pattern string., e.g. **********
The user will input a digit. If the digit is found in the pattern, then the location of where that
digit is at is revealed by changing the * at that location with the digit. For example, if the user
enters a 6 then the program will print *6********. If the digit is not found in the pattern then
do nothing.
If there are more than one occurrences of that digit then reveal the first one. For example, if
the user enters a 7 then the program will print *6*7******. If the user enters another 7 then
the program will print *6*7*7****
Keep repeating until the user has guessed all of the digits
*/

int main(){
  string userSentence;
  string userWord;
  cout << "Enter a sentence: ";
  getline(cin, userSentence);
  cout << "Enter a word: ";
  getline(cin, userWord);
  if (userSentence.find(userWord) != string::npos){
    cout << "word in sentence\n";
  }
  return 0;
}
