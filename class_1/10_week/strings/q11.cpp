#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

/*
Password checking. Write a program for the user to enter a password and then check to see if
the password is valid or not. A valid password must satisfy all of the following criteria:
 at least 10 characters long
 contains at least one digit
 contains at least one lower-case letter
 contains at least one upper-case letter
 contains at least one non-alphanumeric character
 does not contain any consecutive duplicate characters (e.g. "aa")
A prompt with the criteria listed is first printed, followed by the entering of the password. After
entering the password, it is then checked to see whether it satisfies all of the above criteria. If it
does then print out the message “Your password has been accepted.” Otherwise, print out the
message “Your password does not meet the above criteria.”
*/

int main(){
  string userSentence;
  string userWord;
  cout << "Enter a sentence: ";
  getline(cin, userSentence);
  cout << "Enter a word: ";
  getline(cin, userWord);
  if (userSentence.find(userWord) != string::npos){
    cout << "word in sentence\n";
  }
  return 0;
}
