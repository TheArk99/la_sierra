#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

/*
Password checking. Continuing from question 11 above, when the password does not meet
the criteria, the program will also print out which criteria are not satisfied.
*/

int main(){
  string userSentence;
  string userWord;
  cout << "Enter a sentence: ";
  getline(cin, userSentence);
  cout << "Enter a word: ";
  getline(cin, userWord);
  if (userSentence.find(userWord) != string::npos){
    cout << "word in sentence\n";
  }
  return 0;
}
