#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

/*
Write a program to count the words in the following passage. It will output a list of
words and the number of times they appear in the passage. Ignore all punctuation characters
like commas and periods. Treat uppercase and lowercase as different characters. You can, if
you like, treat the word “farmer’s” as two words to make it simpler.
The program will print out the words and their counts like the following

Dorothy - 2
lived - 1
in - 7
the - 11
etc.

Hint: one way to solve this problem is to use two parallel arrays; the first array is of type
string to store the words and the second array is of type int to store the count for the
corresponding word at the same index. For example, the string array at index 0 contains the
word “Dorothy” and the corresponding int array at index 0 will have the count of 2 at the
end. You need to parse the passage for each word. After getting a word, you need to search
through the string array for the word. If it is found then increment the count in the int array
using the same index for where you found the word in the string array. If it is not found then
insert the word into the string array and set the count at the same index in the int array to one.
You'll also need to keep track of how many words you already have added into the string
array (which will be the same as the count array) so that you know where to add the new
word.

Note that your program should work for any given passage and not only for this one. The
program is automatically updating the string array with each new word found and updating
the word count in the int array. You are NOT initializing the arrays manually yourself with
the words and counts.
*/

string replacePunc(string word);


int main(){

  const string passage = "Dorothy lived in the midst of the great Kansas prairies, with Uncle Henry, who was a farmer, and Aunt Em, who was the farmer’s wife. Their house was small, for the lumber to build it had to be carried by wagon many miles. There were four walls, a floor and a roof, which made one room; and this room contained a rusty looking cookstove, a cupboard for the dishes, a table, three or four chairs, and the beds. Uncle Henry and Aunt Em had a big bed in one corner, and Dorothy a little bed in another corner. There was no garret at all, and no cellar except a small hole dug in the ground, called a cyclone cellar, where the family could go in case one of those great whirlwinds arose, mighty enough to crush any building in its path. It was reached by a trap door in the middle of the floor, from which a ladder led down into the small, dark hole.";


  typedef struct {
    int wordCount = 0;
    string word;
  } Words;

  const int SIZE = 100;
  Words words[SIZE];

  int currentElementPassage = 0;
  int currentElementStuct = 0;
  int eow = passage.find(" ");
  string currentWord;
  bool done = false;
  bool foundWord = false;
  do {
    currentWord = currentWord.replace(0,1000,"");
    if (eow == string::npos){
      currentWord = passage.substr(currentElementPassage, passage[passage.length() - 1]);
      currentWord = replacePunc(currentWord);
      //cout << currentWord<<endl;
      done = true;
    }else{
      currentWord = passage.substr(currentElementPassage, eow - currentElementPassage);
      currentWord = replacePunc(currentWord);
      //cout << currentWord<<endl;
    }
    int i;
    for (i = 0; i < currentElementStuct; i++){
      if (words[i].word == currentWord){
        foundWord = true;
        break;
      }
    }
    if (foundWord == true){
      words[i].wordCount++;
      foundWord = false;
    }else{
      words[i].word = currentWord;
      currentElementStuct++;
    }

    currentElementPassage = eow + 1;
    eow = passage.find(" ", currentElementPassage);
  }while(done == false);

  for (int i = 0; i < currentElementStuct; i++){
    cout << words[i].word << " - " <<words[i].wordCount + 1<<endl;
  }
  return 0;
}


//func to get rid of all punctuation
string replacePunc(string word){
  if (ispunct(word[word.length() - 1]) == true){
    word.replace(word.length() -1, word.length(), "");
  }
  return word;
}
