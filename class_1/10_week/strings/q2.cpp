#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

/*
Write a program to input a c-string, and then use a loop to count to find out the length of the
string, i.e. without using the strlen() function. Compare your count with the number returned
by the strlen() function to see if they are the same. Print out appropriate messages.
*/

int main(){
  char *userString;
  int count = 0;
  cin.getline(userString, strlen(userString));
  int i = 0;
  while (userString[i] != '\0'){
      count++;
      i++;
  }
  cout << "count is: "<< count << endl;
  cout << "strlen is: "<< strlen(userString)<<endl;
  return 0;
}
