#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

/*
Write a program to input a c-string, and then print out how many lower case, how many
upper case, and how many printable non-alphanumeric characters are in the c-string.
*/

int main(){
  char *userString;
  int countLower = 0;
  int countUpper = 0;
  int countSymbol = 0;
  cin.getline(userString, strlen(userString));
  for (int i = 0; i < strlen(userString); i++){
    if (islower(userString)){
      countLower++;
    }
  }
  return 0;
}
