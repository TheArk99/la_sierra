#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

/*
Write a program to input a sentence using a c-string. Print out how many words are in the
sentence. Assume that all the words are separated by exactly one space. You need to use a
loop to scan through the entire c-string array.
*/

int main(){
  char *userString;
  int countLower = 0;
  int countUpper = 0;
  int countSymbol = 0;
  cin.getline(userString, strlen(userString));
  for (int i = 0; i < strlen(userString); i++){
    if (islower(userString)){
      countLower++;
    }
  }
  return 0;
}
