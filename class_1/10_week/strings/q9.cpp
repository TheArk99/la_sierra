#include <stdio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

/*
Write a program to first input a sentence using the string type. Then input a word. Print out a
message saying whether the word is in the sentence or not. You need to use of some of the
string library functions.
*/

int main(){
  string userSentence;
  string userWord;
  cout << "Enter a sentence: ";
  getline(cin, userSentence);
  cout << "Enter a word: ";
  getline(cin, userWord);
  if (userSentence.find(userWord) != string::npos){
    cout << "word in sentence\n";
  }
  return 0;
}
