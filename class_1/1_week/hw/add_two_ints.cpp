/* Noah McMillan
 * Homework 1
 * Question 5
 * Program to add two stdin ints and then print
 * September 27, 2023
 */
#include <iostream>
using namespace std;
int main() {
  int first_int, second_int;

  cout << "Enter first interger: ";
  cin >> first_int;

  cout << "Enter second interger: ";
  cin >> second_int;

  cout << "The sum of the two ints is: ";
  cout << first_int + second_int << "\n";

  return 0;
}
