/* Noah McMillan
 * Homework 1
 * Question 8
 * Program to take input then make my address the output
 * September 27, 2023
 */


#include <iostream>
using namespace std;


struct {
  string name;
  string street;
  string city;
} personal_info;

int main(){
  cout << "Enter Name:\n";
  getline(cin, personal_info.name);

  cout << "Enter street:\n";
  getline(cin, personal_info.street);

  cout << "Enter city:\n";
  getline(cin, personal_info.city);

  cout << endl;
  cout << personal_info.name << endl;
  cout << personal_info.street << endl;
  cout << personal_info.city << endl;

  return 0;
}
