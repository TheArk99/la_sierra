/* Noah McMillan
 * Homework 1
 * Question 7
 * Program to find quotient of two stdin floats
 * September 27, 2023
 */
#include <iostream>
using namespace std;
int main() {
  float first_float;
  float second_float;

  cout << "Enter first float:\n";
  cin >> first_float;

  cout << "Enter second float:\n";
  cin >> second_float;

  cout.setf(ios::fixed); // use fixed format
  cout.setf(ios::showpoint); // use a decimal
  cout.precision(3); // two digits after the decimal point

  cout << first_float / second_float;

  return 0;
}
