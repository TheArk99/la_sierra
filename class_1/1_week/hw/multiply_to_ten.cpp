/* Noah McMillan
 * Homework 1
 * Question 10
 * Program to multiply up to 10
 * September 28, 2023
 */
#include <iostream>
using namespace std;
int main() {
  int user_int;

  cout << "Enter int to multiple up to ten: ";
  cin >> user_int;

//  for (int i = 1; i < 11; i++) {
 //   cout << i << " x " << user_int << " = " << i * user_int << endl;
 // }
  cout << 1 << " x " << user_int << " = " << 1 * user_int << endl;
  cout << 2 << " x " << user_int << " = " << 2 * user_int << endl;
  cout << 3 << " x " << user_int << " = " << 3 * user_int << endl;
  cout << 4 << " x " << user_int << " = " << 4 * user_int << endl;
  cout << 5 << " x " << user_int << " = " << 5 * user_int << endl;
  cout << 6 << " x " << user_int << " = " << 6 * user_int << endl;
  cout << 7 << " x " << user_int << " = " << 7 * user_int << endl;
  cout << 8 << " x " << user_int << " = " << 8 * user_int << endl;
  cout << 9 << " x " << user_int << " = " << 9 * user_int << endl;
  cout << 10 << " x " << user_int << " = " << 10 * user_int << endl;

  return 0;
}
