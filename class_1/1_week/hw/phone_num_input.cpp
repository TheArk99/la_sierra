/* Noah McMillan
 * Homework 1
 * Question 9
 * Program to take input then make phone nums formated
 * September 27, 2023
 */


#include <iostream>
#include <iomanip>
using namespace std;


//struct {
//  string name[3];
//  string phone[3];
//} personal_info;

int main(){
 // for(int i = 0; i < 3; i++){
//    cout << "Enter Name: ";
   // getline(cin, personal_info.name[i]);
   // cout << "Enter phne num: ";

  //  getline(cin, personal_info.phone[i]);
  //}

  string personal_info_name1;
  string personal_info_name2;
  string personal_info_name3;

  cout << "Enter first name: ";
  getline(cin, personal_info_name1);

  cout << "Enter second name: ";
  getline(cin, personal_info_name2);

  cout << "Enter third name: ";
  getline(cin, personal_info_name3);

  string personal_info_phone1;
  string personal_info_phone2;
  string personal_info_phone3;

  cout << "Enter first phone: ";
  getline(cin, personal_info_phone1);

  cout << "Enter second phone: ";
  getline(cin, personal_info_phone2);

  cout << "Enter third phone: ";
  getline(cin, personal_info_phone3);

  cout << endl;

  cout << personal_info_name1;
  cout << setw(15) << personal_info_phone1 << endl;

  cout << personal_info_name2;
  cout << setw(15) << personal_info_phone2 << endl;

  cout << personal_info_name3;
  cout << setw(15) << personal_info_phone3 << endl;

  //cout << endl;

  //for(int i = 0; i < 3; i++){
   // cout << personal_info.name[i];
    //cout << setw(15) << personal_info.phone[i] << endl;
  //}

  return 0;
}
