/* Noah McMillan
 * Homework 1
 * Question 1
 * simplifying cout
 * September 27, 2023
 */

#include <iostream>
using namespace std;
int main() {
  //original:
  //cout << "Hello, " << "I am " << "a C++ statement";
  cout << "Hello, I am a C++ statement";

  return 0;
}
