/* Noah McMillan
 * Homework 1
 * Question 2 and 3
 * Program to test cin command
 * September 27, 2023
 */
#include <iostream>
using namespace std;
int main() {
  int input;
  cin >> input;
  cout << input;

  return 0;
}
