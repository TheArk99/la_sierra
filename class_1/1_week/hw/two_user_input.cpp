/* Noah McMillan
 * Homework 1
 * Question 4
 * Program to get two stdin then print them in secuetial order
 * September 27, 2023
 */
#include <iostream>
using namespace std;
int main() {
  string first_input;
  string second_input;

  cout << "Give first fruit:\n";
  cin >> first_input;
  cout << "Give second fruit:\n";
  cin >> second_input;

  cout << "I like to eat " << first_input << " but not " << second_input << "!\n";


  return 0;
}
