/* Noah McMillan
 * Lab 1
 * Question 3
 * Program to make a box 5x5
 * September 27, 2023
 */

#include <iostream>
using namespace std;



int main(){
  for(int i=0;i < 5;i++){
    cout << "*";
  }
  cout << "\n";

  for(int i=0;i < 5;i++){
    cout << "*";
  }

  cout << "\n";

  for(int i=0;i < 5;i++){
    cout << "*";
  }

  cout << "\n";

  for(int i=0;i < 5;i++){
    cout << "*";
  }

  cout << "\n";

  for(int i=0;i < 5;i++){
    cout << "*";
  }

  cout << "\n";

  return 0;
}
