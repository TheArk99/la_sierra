/* Noah McMillan
 * Lab 1
 * Question 6
 * Program to make a address format
 * September 27, 2023
 */

#include <iostream>
using namespace std;


int main(){
  cout << "John Doe\n";
  cout << "123 This Street\n";
  cout << "Riverside, CA 92505\n";

  cout << "\n\n\t\t\t\tJane Doe\n";
  cout << "\t\t\t\t123 This Street\n";
  cout << "\t\t\t\tRiverside, CA 92505\n";



  return 0;
}
