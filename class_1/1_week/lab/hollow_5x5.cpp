/* Noah McMillan
 * Lab 1
 * Question 4
 * Program to make a box 5x5 that is hollow
 * September 27, 2023
 */

#include <iostream>
using namespace std;



int main(){
  for(int i=0;i < 5;i++){
    cout << "*";
  }
  cout << "\n*   *\n*   *\n*   *\n*";

  for(int i=0;i < 4;i++){
    cout << "*";
  }

  cout << "\n";

  return 0;
}
