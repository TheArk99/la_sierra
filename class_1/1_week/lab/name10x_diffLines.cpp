/* Noah McMillan
 * Lab 1
 * Question 2
 * Program to make a name 10x with each line
 * September 27, 2023
 */

#include <iostream>
using namespace std;



int main(){
  for(int i=0;i < 10;i++){
    cout << "noah\n";
  }

  return 0;
}
