/* Noah McMillan
 * Lab 1
 * Question 5
 * Program to make a triangle
 * September 27, 2023
 */

#include <iostream>
using namespace std;



int main(){
  cout << "*\n";
  cout << "**\n";
  cout << "***\n";
  cout << "****\n";
  cout << "*****\n";

  return 0;
}
