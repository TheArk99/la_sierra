/* Noah McMillan
 * lab 2
 * Question 1
 * Program to calculate autmobile prices
 * October 04, 2023
 */

//things to calc: loan payment, insurance, gas, oil, tires,
//and maintenance

#include <iostream>
using namespace std;
int main() {

  float loan;
  float insurance;
  float gas;
  float oil;
  float tires;
  float maintenance;

  cout << "Enter monthly loan payment: ";
  cin >> loan;

  cout << "Enter monthly insurance payment: ";
  cin >> insurance;

  cout << "Enter monthly gas payment: ";
  cin >> gas;

  cout << "Enter monthly oil payment: ";
  cin >> oil;

  cout << "Enter monthly tires payment: ";
  cin >> tires;

  cout << "Enter monthly maintenance payment: ";
  cin >> maintenance;

  float totalMonthly = maintenance + tires + oil + gas + insurance + loan;
  float totalYearly = totalMonthly * 12;

  cout << "\nYour total monthly payment is: $" << totalMonthly;
  cout << "\nYour total Yearly payment is: $" << totalYearly << "\n";

  return 0;
}
