/* Noah McMillan
 * lab 2
 * Question 10
 * Program that calculates how many widgets are stacked on a pallet, based on the total weight of the pallet. The program should ask the user how much the pallet weighs by itself and how much it weights with the widgets stacked on it
 * October 04, 2023
 */

//The Yukon Widget Company manufactures widgets that weigh 12.5 pounds each. Write a
//program that calculates how many widgets are stacked on a pallet, based on the total
//weight of the pallet. The program should ask the user how much the pallet weighs by
//itself and how much it weights with the widgets stacked on it. It should then calculate and
//display the number of widgets stacked on the pallet.

#include <iostream>
using namespace std;


const float WIDGET_WEIGHT = 12.5;

int main() {
  float palletWeight;
  float totalWeight;
  float numWidget;

  cout << "Enter weight of pallet: ";
  cin >> palletWeight;

  cout << "Enter total weight of stack (not heap lol, first in last out) on pallet: ";
  cin >> totalWeight;


  numWidget = (totalWeight - palletWeight) / WIDGET_WEIGHT;

  cout << "The total number of widgets is: " << numWidget << "\n";

  return 0;
}
