/* Noah McMillan
 * lab 2
 * Question 11
 * Program that asks the user to input how many cookies he or she actually ate, then reports how many total calories were consumed.
 * October 04, 2023
 */

//A bag of cookies holds 30 cookies. The calorie information on the bag claims there are 10 "servings" in the bag and that a serving equals 300 calories.

const float COOKIES_SERVINGS = 10;
const float COOKIES_SERVINGS_CALORIES = 300;

#include <iostream>
using namespace std;
int main() {
  float cookiesEat;

  cout << "Enter how many cookies eaten: ";
  cin >> cookiesEat;


  float totalCaloriesEaten = (cookiesEat * 300) / 10;

  cout << "The total calories eaten are: " << totalCaloriesEaten << "\n";

  return 0;
}
