/* Noah McMillan
 * lab 2
 * Question 12
 * Program that asks the user to enter the replacement cost of a building, and then displays the minimum amount of insurance he or she should buy for the property
 * October 04, 2023
 */

const float INSURE_PERCENT = 0.80;

#include <iostream>
using namespace std;
int main() {
  float replaceCost;

  cout << "Enter the replacement cost of a building: ";
  cin >> replaceCost;


  float amountInsure = replaceCost * .80;

  cout << "The minimum amount of insurance you should buy for the property is: $" << amountInsure << "\n";

  return 0;
}
