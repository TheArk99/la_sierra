/* Noah McMillan
 * lab 2
 * Question 2
 * Program that will convert U.S. dollar amounts to Japanese yen and to Euros
 * October 04, 2023
 */

const float YEN_PER_DOLLAR = 149.03;
const float EUROS_PER_DOLLAR = 0.95;

#include <iostream>
using namespace std;
int main() {
  float dollars;

  cout << "Enter dollar amount: ";
  cin >> dollars;

  float yen = dollars * YEN_PER_DOLLAR;
  float euros = dollars * EUROS_PER_DOLLAR;

  cout.setf(ios::fixed); // use fixed format
  cout.setf(ios::showpoint); // use a decimal
  cout.precision(2); // two digits after the decimal point
  cout << "The dollar amount $" << dollars << " is equivalant to: " << "\n";
  cout << yen << " yen\n";
  cout << euros << " euros\n";

  return 0;
}
