/* Noah McMillan
 * lab 2
 * Question 2
 * Program to get average reuslt
 * October 04, 2023
 */
#include <iostream>
using namespace std;
int main() {
  float test_result_1;
  float test_result_2;
  float test_result_3;
  float test_result_4;
  float test_result_5;

  cout << "Enter test result 1: ";
  cin >> test_result_1;

  cout << "Enter test result 2: ";
  cin >> test_result_2;

  cout << "Enter test result 3: ";
  cin >> test_result_3;

  cout << "Enter test result 4: ";
  cin >> test_result_4;

  cout << "Enter test result 5: ";
  cin >> test_result_5;

  float result = (test_result_1 + test_result_2 + test_result_3 + test_result_4 + test_result_5) / 5;

  cout << "The average test results are: " << result << "\n";

  return 0;
}
