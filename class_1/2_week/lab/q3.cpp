/* Noah McMillan
 * lab 2
 * Question 3
 * Program to
 * October 04, 2023
 */
#include <iostream>
using namespace std;
int main() {
  const int CLASS_A_PRICE = 15.00;
  const int CLASS_B_PRICE = 12.00;
  const int CLASS_C_PRICE = 9.00;
  int classApurchased;
  int classBpurchased;
  int classCpurchased;

  cout << "Enter the number of Class A tickets sold: ";
  cin >> classApurchased;

  cout << "Enter the number of Class B tickets sold: ";
  cin >> classBpurchased;

  cout << "Enter the number of Class C tickets sold: ";
  cin >> classCpurchased;

  int classAtotalMoney = classApurchased * CLASS_A_PRICE;
  int classBtotalMoney = classBpurchased * CLASS_B_PRICE;
  int classCtotalMoney = classCpurchased * CLASS_C_PRICE;

  int totalMoneyMade = classAtotalMoney + classBtotalMoney + classCtotalMoney;

  cout << "The total money made was: $" << totalMoneyMade << "\n";

  return 0;
}
