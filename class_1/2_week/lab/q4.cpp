/* Noah McMillan
 * lab 2
 * Question 4
 * Program to calculates a car’s gas mileage
 * October 04, 2023
 */
#include <iostream>
using namespace std;
int main() {
  float gallonsPerHold;
  float milesPerFull;

  cout << "Enter number of gallons of gas the car can hold: ";
  cin >> gallonsPerHold;

  cout << "Enter how many miles the car can be driven on full: ";
  cin >> milesPerFull;


  float result = milesPerFull / gallonsPerHold;

  cout << "The car's miles per Gallon is: " << result << "\n";

  return 0;
}
