/* Noah McMillan
 * lab 2
 * Question 5
 * Program that converts Celsius temperatures to Fahrenheit temperatures
 * October 04, 2023
 */

// F = 9/5 * C + 32

#include <iostream>
using namespace std;
int main() {
  float celsius;
  float fahrenheitConversion;

  cout << "Enter Celsius temp to convert: ";
  cin >> celsius;

  fahrenheitConversion = ((9 * celsius) / 5)+ 32;

  cout << "The temp from C to F is: " << fahrenheitConversion << "\n";

  return 0;
}
