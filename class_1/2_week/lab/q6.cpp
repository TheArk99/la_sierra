/* Noah McMillan
 * lab 2
 * Question 6
 * Program  that calculates the average rainfall for three months
 * October 04, 2023
 */
//The program should
//ask the user to enter the name of each month, such as June or July, and the amount of rain
//(in inches) that fell each month. The program should display a message similar to the
//following:
//The average rainfall for June, July, and August is 6.72 inches.

#include <iostream>
using namespace std;
int main() {
  string month_1_name;
  float month_1_rainfall;

  string month_2_name;
  float month_2_rainfall;

  string month_3_name;
  float month_3_rainfall;

  cout << "Enter the name of month 1: ";
  cin >> month_1_name;
  cout << "Enter the rainfall for month 1: ";
  cin >> month_1_rainfall;

  cout << "Enter the name of month 2: ";
  cin >> month_2_name;
  cout << "Enter the rainfall for month 2: ";
  cin >> month_2_rainfall;

  cout << "Enter the name of month 3: ";
  cin >> month_3_name;
  cout << "Enter the rainfall for month 3: ";
  cin >> month_3_rainfall;

  float totalRainfall = month_1_rainfall + month_2_rainfall + month_3_rainfall;

  cout << "\nThe average rainfall for: " << month_1_name;
  cout << ", " << month_2_name;
  cout << ", and " << month_3_name;
  cout << " is " << totalRainfall << " inches.\n";

  return 0;
}
