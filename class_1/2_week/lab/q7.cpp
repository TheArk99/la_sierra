/* Noah McMillan
 * lab 2
 * Question 7
 * Program that asks the user for the number of males and the number of females
 * registered in a class
 * October 04, 2023
 */
#include <iostream>
using namespace std;
int main() {
  float num_of_females;
  float num_of_males;

  cout << "Enter number of females in the class: ";
  cin >> num_of_females;

  cout << "Enter number of males in the class: ";
  cin >> num_of_males;

  float total_num_students = num_of_females + num_of_males;

  float percent_of_males = (num_of_males / total_num_students) * 100;
  float percent_of_females = (num_of_females / total_num_students) * 100;

  cout.setf(ios::fixed); // use fixed format
  cout.setf(ios::showpoint); // use a decimal
  cout.precision(1); // two digits after the decimal point

  cout << "The percents of males and females are: \n";
  cout << "Males: %" << percent_of_males << "\n";
  cout << "Females: %" << percent_of_females << "\n";

  return 0;
}
