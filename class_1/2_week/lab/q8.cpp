/* Noah McMillan
 * lab 2
 * Question 8
 * Program that asks the user how many cookies he or she wants to make, then displays the number of cups of each ingredient needed for the specified number of cookies.
 * October 04, 2023
 */

//A cookie recipe calls for the following ingredients:
// 1.5 cups of sugar
// 1 cup of butter
// 2.75 cups of flour
//The recipe produces 48 cookies with this amount of the ingredients
//hint:  If the user wants n cookies then to calculate the amount of sugar needed, you do: n * 1.5/48

#include <iostream>
using namespace std;
int main() {
  float requestedCookies;

  cout << "Enter how many cookies you want: ";
  cin >> requestedCookies;

  float cupsSugar = (requestedCookies * 1.5) / 48;
  float cupsButter = (requestedCookies * 1) / 48;
  float cupsFlour = (requestedCookies * 2.75) / 48;

  cout << "\nThe num of cups of each ingredient needed for: " << requestedCookies << " cookies\n";
  cout << cupsSugar << " cups of sugar needed\n";
  cout << cupsButter << " cups of butter needed\n";
  cout << cupsFlour << " cups of Flour needed\n";

  return 0;
}
