/* Noah McMillan
 * lab 2
 * Question 9
 * Program that calculates a theater's gross and net box office profit for a night
 * October 04, 2023
 */

// Assume the theater keeps 20 percent of the gross box office profit. The program should ask for the name of the movie, and how many adult and child tickets were sold. (The price of an adult ticket is $10.00 and a child's ticket is $6.00.) It should display a report similar to:
//Movie Name: “Wheels of Fury”
//Adult Tickets Sold: 382
//Child Tickets Sold: 127
//Gross Box Office Profit: $4,582.00
//Net Box Office Profit: $916.40
//Amount Paid to Distributor: $3,665.60

#include <iostream>
#include <sstream>
using namespace std;

const float ADULT_TICKET_PRICE = 10.00;
const float CHILD_TICKET_PRICE = 6.00;
const float PERCENT_TO_DISTRIB = 0.20;

int main() {
  string movieName;
  string numAdultStr; //temp str var to allow casting to float from getline
  string numChildStr;//temp str var to allow casting to float from getline
  float numAdult;
  float numChild;
  float grossProfit;
  float netProfit;
  float paidDistrib;
  float totalAdultMoney;
  float totalChildMoney;

  cout << "Enter the name of the movie: ";
  getline(cin, movieName);

  cout << "Enter number of Audlt tickets sold: ";
  getline(cin, numAdultStr);

  cout << "Enter number of Child tickets sold: ";
  getline(cin, numChildStr);

  stringstream(numAdultStr) >> numAdult; //casts str to float
  stringstream(numChildStr) >> numChild;//casts str to float

  totalAdultMoney = numAdult * ADULT_TICKET_PRICE;
  totalChildMoney = numChild * CHILD_TICKET_PRICE;

  grossProfit = totalChildMoney + totalAdultMoney;
  netProfit = grossProfit * PERCENT_TO_DISTRIB;
  paidDistrib = grossProfit - netProfit;

  cout << "\nMovie name: " << movieName << "\n";
  cout << "Adult Tickets Sold: " << numAdult << "\n";
  cout << "Child Tickets Sold: " << numChild << "\n";
  cout << "Gross Box Office Profit: $" << grossProfit << "\n";
  cout << "Net Box Office Profit: $" << netProfit << "\n";
  cout << "Amount Paid to Distributor: $" << paidDistrib << "\n";

  return 0;
}
