/* Noah McMillan
 * If statement exercises
 * Question 1
 * Program
 * October 9, 2023
 */

/*
Write a program to enter two numbers, num1 and num2. num1 is divided by num2 and the
result is displayed. Before the division, num2 is tested for the value 0. If it is a 0, the
division does not take place with an appropriate message.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float num1, num2;
  cout << "Enter num for num1: ";
  cin >> num1;
  cout << "Enter num for num2: ";
  cin >> num2;

  if (num2 == 0){
    cout << "\nERROR!!! NUM2 CAN NOT EQUAL 0\n";
    return -1;
  }


  cout << "num1 / num2 is: " << num1 / num2 << endl;

  return 0;
}
