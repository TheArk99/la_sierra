/* Noah McMillan
 * If statement exercises
 * Question 10
 * Program
 * October 10, 2023
 */

/*
Write a program that displays the following menu:
1. Calculate the area of a circle
2. Calculate the area of a rectangle
3. Calculate the area of a triangle
4. Quit
Enter your choice (1-4)?
Your program will ask for additional information and perform the appropriate calculation
depending on the user choice.
 Formula Additional user input
The area of a circle is pi(r^2) (use 3.14159 for pi) radius
The area of a rectangle is length * width length and width
The area of a triangle is base * height / 2 base and height
*/

#include <iostream>
using namespace std;


const float pi = 3.14159;

int main(int argc, char **argv) {
  unsigned int user_choice;
  float radians, length, width, base, height;

  cout << "1. Calculate the area of a circle\n";
  cout << "2. Calculate the area of a rectangle\n";
  cout << "3. Calculate the area of a triangle\n";
  cout << "4. Quit\n";
  cout << "... ";

  cin >> user_choice;

/*
  switch (user_choice){
    case 1:
      cout << "Enter radians to calc: ";
      cin >> radians;
      cout << "\nThe area of a circle is: " << pi * (pow(radians, 2)) << "\n";
      break;
    case 2:
      cout << "Enter length to calc: ";
      cin >> length;
      cout << "Enter width to calc: ";
      cin >> width;
      cout << "\nThe area of a rectangle is: " << length * width << "\n";
      break;
    case 3:
      cout << "Enter base to calc: ";
      cin >> base;
      cout << "Enter height to calc: ";
      cin >> height;
      cout << "\nThe area of a triangle is: " << (base * height) / 2 << "\n";
      break;
    case 4:
    cout << "Quiting...\n";
    break;
    default:
      cout << "ERROR NO CORRECT CHOICE GIVEN\n";
      return -1;
  }
*/


  if (user_choice == 1){
      cout << "Enter radians to calc: ";
      cin >> radians;
      cout << "\nThe area of a circle is: " << pi * (pow(radians, 2)) << "\n";
  }
  else if (user_choice == 2){
      cout << "Enter length to calc: ";
      cin >> length;
      cout << "Enter width to calc: ";
      cin >> width;
      cout << "\nThe area of a rectangle is: " << length * width << "\n";
  }
  else if (user_choice == 3){
      cout << "Enter base to calc: ";
      cin >> base;
      cout << "Enter height to calc: ";
      cin >> height;
      cout << "\nThe area of a triangle is: " << (base * height) / 2 << "\n";
  }
  else if (user_choice == 4){
    cout << "Quiting...\n";
  }
  else{
      cout << "ERROR NO CORRECT CHOICE GIVEN\n";
      return -1;
  }

  return 0;
}
