/* Noah McMillan
 * If statement exercises
 * Question 11
 * Program
 * October 10, 2023
 */

/*
Write a program to enter three numbers and then print out the three numbers in
ascending order. Hint 1: First put the first two numbers on a number line. (What I mean
by “putting on a number line” is to know which of the two numbers is smaller, just like
question 2.) Once you have the first two numbers on the number line then you will only
have three different options or three ranges in which the third number can be in.
-----------------------1st#-----------------------2nd#-----------------------
 range1 range2 range3
So you can now test to see which range the third number is in. After knowing the range
that the third number is in, you will know the order to print these three numbers out.
*/

#include <iostream>
using namespace std;


void swapNums(int &x, int &y){
  int tempVar = x;
  x = y;
  y = tempVar;
}

/*
void sort(int user_nums[], int n){
  for (int i = 0; i < n - 1; i++){
    for (int j = 0; j < n - i - 1; j++){
      if (user_nums[j] > user_nums[j+1]){
        swapNums(user_nums[j], user_nums[j+1]);
      }
    }
  }
}
*/

int main(int argc, char **argv) {
  int user_nums[3];

  cout << "Enter First num: ";
  cin >> user_nums[0];
  cout << "Enter Second num: ";
  cin >> user_nums[1];
  cout << "Enter Thrid num: ";
  cin >> user_nums[2];

  /*
  int n = sizeof(user_nums)/sizeof(user_nums[0]);

  sort(user_nums, n);

  */


  if (user_nums[0] > user_nums[1]){
    swapNums(user_nums[0], user_nums[1]);
    if (user_nums[1] > user_nums[2]){
      swapNums(user_nums[1], user_nums[2]);
      if (user_nums[0] > user_nums[1]){
        swapNums(user_nums[0], user_nums[1]);
      }
    }
  }
  else if (user_nums[1] > user_nums[2]){
    swapNums(user_nums[1], user_nums[2]);
    if (user_nums[0] > user_nums[1]){
      swapNums(user_nums[0], user_nums[1]);
    }
  }

  cout << "\nuser_nums[0] is: " << user_nums[0] << endl;
  cout << "\nuser_nums[1] is: " << user_nums[1] << endl;
  cout << "\nuser_nums[2] is: " << user_nums[2] << endl;

  return 0;
}
