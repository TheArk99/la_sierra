/* Noah McMillan
 * If statement exercises
 * Question 2
 * Program
 * October 9, 2023
 */

/*
Write a program to enter two numbers and then print out the two numbers in ascending
order.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float num1, num2;
  cout << "Enter num for num1: ";
  cin >> num1;
  cout << "Enter num for num2: ";
  cin >> num2;

  if (num2 < num1){
    cout << "num2 is: " << num2 << "\nnum1 is: " << num1  << endl;
  }
  else{
    cout << "num1 is: " << num1 << "\nnum2 is: " << num2 << endl;
  }

  return 0;
}
