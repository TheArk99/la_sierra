/* Noah McMillan
 * If statement exercises
 * Question 3
 * Program
 * October 9, 2023
 */

/*
Write a program to enter a temperature. If the temperature is less than 20 or greater than
100, then print the message “The temperature is in the danger zone.”
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float temp;
  cout << "Enter temp: ";
  cin >> temp;

  if (temp > 100 || temp < 20){
    cout << "The temperature is in the danger zone." << endl;
  }

  return 0;
}
