/* Noah McMillan
 * If statement exercises
 * Question 4
 * Program
 * October 9, 2023
 */

/*
Just like 3 but in addition if the temperature is not in the danger zone then print the
message “The temperature is fine.”
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float temp;
  cout << "Enter temp: ";
  cin >> temp;

  if (temp > 100 || temp < 20){
    cout << "The temperature is in the danger zone." << endl;
  }
  else{
    cout << "The temperature is fine." << endl;
  }

  return 0;
}
