/* Noah McMillan
 * If statement exercises
 * Question 5
 * Program
 * October 9, 2023
 */

/*
Write a program to enter three numbers representing the length of the three sides of a
triangle. Print out whether the three lengths can form a valid triangle or not. Hint: the sum
of two side lengths of a triangle is always greater than the third side. If this is true for all
three combinations of added side lengths, then you will have a triangle.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float sideA;
  float sideB;
  float sideC;

  cout << "Enter triangle sideA: ";
  cin >> sideA;
  cout << "Enter triangle sideB: ";
  cin >> sideB;
  cout << "Enter triangle sideC: ";
  cin >> sideC;

  if (sideA + sideB > sideC && sideB + sideC > sideA && sideC + sideA > sideB){
    cout << "The triangle is valid!!!" << endl;
  }
  else{
    cout << "The triangle is not valid." << endl;
  }

  return 0;
}
