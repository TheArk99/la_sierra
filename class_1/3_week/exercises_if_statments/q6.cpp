/* Noah McMillan
 * If statement exercises
 * Question 6
 * Program
 * October 9, 2023
 */

/*
To qualify for a certain government grant, a person must have worked more than 5 years
and earn at least $35,000. Write a program to enter the number of years a person has
worked and his/her annual income, then print out whether the person qualifies for the
grant or not. You need to print out one of the following messages:
“You qualify for the government grant”
“You do not qualify because you need to work for more than 5 years”
“You do not qualify because you need to earn more than $35,000”
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  unsigned int yearsWorked;
  unsigned int salary;

  cout << "Enter the num of years worked: ";
  cin >> yearsWorked;
  cout << "Enter salary: ";
  cin >> salary;

  if (yearsWorked > 5 && salary >= 35000){
    cout << "You qualify for the government grant" << endl;
  }
  else if (yearsWorked <= 5 && salary >= 35000){
    cout << "You do not qualify because you need to work for more than 5 years" << endl;
  }
  else if (yearsWorked > 5 && salary < 35000){
    cout << "You do not qualify because you need to earn more than $35,000" << endl;
  }
  else{
    cout << "You do not qualify because you need to earn more than $35,000 and have more than 5 years worked" << endl;
//    cout << "ERROR" << endl;
//    return -1;
  }

  return 0;
}
