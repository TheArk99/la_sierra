/* Noah McMillan
 * If statement exercises
 * Question 7
 * Program
 * October 9, 2023
 */

/*
Write a program to generate two random numbers between 0 and 100. Ask the user to
enter the answer for the sum of the two numbers. Print out whether the answer entered by
the user is correct or not. See the Random Number document on how to generate random
numbers.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int check_user(){
  long user_guess = 0;
  while (user_guess == 0) {
    cin >> user_guess;
    if ( user_guess > 300){
      user_guess = 0;
      cout << "ENTER CORRECT INPUT\n";
    }
  }
  return user_guess;
}

int main(int argc, char **argv) {
  unsigned long seed = time(0);
  srand(seed);

  int rand_num_1 = rand() % 101;
  int rand_num_2 = rand() % 101;

  printf("What does %d + %d = ?: ", rand_num_1, rand_num_2);

  bool user_completion;

  while (user_completion != true){
    if (check_user() == rand_num_1 + rand_num_2){
      cout << "CORRECT!\n";
      user_completion = true;

    }
    else {
      cout << "INCORRECT!!!\nTry again?[y/n]: ";

      string yN;
      cin >> yN;

      if (yN == "y" || yN == "Y"){
          continue;
      }
      else if (yN == "n" || yN == "N"){
          user_completion = true;
      }
      else{
          continue;
      }
    }
  }

  return 0;
}
