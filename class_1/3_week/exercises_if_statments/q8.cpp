/* Noah McMillan
 * If statement exercises
 * Question 8
 * Program
 * October 9, 2023
 */

/*
Write a program to enter a numeric grade which is an integer between 0 and 100. The
program will then print out the corresponding letter grade as follows:
90 to 100 A
80 to 89 B
70 to 79 C
60 to 69 D
50 to 59 F
 Hint: need to use multiple ELSE IF statements.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  unsigned int grade;

  cout << "Enter the grade: ";
  cin >> grade;

  if (grade >= 90){
    cout << "The score got you an A" << endl;
  }
  else if (grade >= 80 && grade <= 89){
    cout << "The score got you an B" << endl;
  }
  else if (grade >= 70 && grade <= 79){
    cout << "The score got you an C" << endl;
  }
  else if (grade >= 60 && grade <= 69){
    cout << "The score got you an D" << endl;
  }
  else{
    cout << "The score got you an F" << endl;
  }

  return 0;
}
