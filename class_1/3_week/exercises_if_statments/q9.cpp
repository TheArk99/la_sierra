/* Noah McMillan
 * If statement exercises
 * Question 9
 * Program
 * October 9, 2023
 */

/*
Write a program to enter how many books are being purchased. Depending on the
number of books purchased, a customer gets a certain percentage discount as follows:
5 or more books 20%
3 to 4 books 10%
Less than 3 books 5%
Print out the percentage discount that the customer gets.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  unsigned int booksPurchased;

  cout << "Enter the num of books purchased: ";
  cin >> booksPurchased;

  if (booksPurchased >= 5){
    cout << "You got 20\% off" << endl;
  }
  else if (booksPurchased >= 3 && booksPurchased <= 4){
    cout << "You got 10\% off" << endl;
  }
  else if (booksPurchased < 3){
    cout << "You got 5\% off" << endl;
  }
  else{
    cout << "ERROR" << endl;
    return -1;
  }

  return 0;
}
