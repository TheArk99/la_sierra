/* Noah McMillan
 * Homework 2
 * Question 1
 * Program
 * October 9, 2023
 */

//How do you declare three int variables month, day, and year in one statement line, with month
//initialized to 10 and year initialized to 2023

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  int month = 10, year = 2023, day;
  cout << "the month is: " << month << "\nthe year is: " << year << endl;
  return 0;
}
