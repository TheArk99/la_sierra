/* Noah McMillan
 * Homework 2
 * Question 2
 * Program
 * October 9, 2023
 */

//Write assignment statements that perform the following operations with the variables x, y, and z:
//a. Adds 2 to x and stores the result in y.
//b. Multiples y by 4 and stores the result in x.
//c. Divides z by 3.14 and stores the result in y.
//d. Stores the value 27 in z.
//e. Stores the square root of y into z.

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  float x, y, z;
  x = 0;
  z = 1;
  y = 2 + x;
  x = y * 4;
  y = z / 3.14;
  z = 27;
  z = sqrt(y);
  cout << "x: " << x << "\ny: " << y << "\nz: " << z << endl;
  return 0;
}
