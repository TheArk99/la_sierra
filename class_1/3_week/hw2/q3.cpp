/* Noah McMillan
 * Homework 2
 * Question 3
 * Program
 * October 9, 2023
 */

//Write a program to input the radius of a circle, and then print out the area and circumference of the circle. The area for a circle is πr^2 and the circumference is 2πr where r is the radius. Use the math library function calls where possible.

#include <iostream>
using namespace std;


const float pi = 3.1415;

int main(int argc, char **argv) {
  float radius, area, circumference;
  cout << "Enter the radius: ";
  cin >> radius;

  area = pi * (pow(radius, 2));
  circumference = 2 * pi * radius;

  cout << "The area is: " << area << "\ncircumference is: " << circumference << endl;

  return 0;
}
