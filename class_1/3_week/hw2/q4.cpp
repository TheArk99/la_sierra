/* Noah McMillan
 * Homework 2
 * Question 4
 * Program
 * October 9, 2023
 */

//Write a program to generate 20 random numbers between 1 and 8. Display these 20 numbers, and the sum of these 20 numbers.

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main(int argc, char **argv) {
  unsigned int seed = time(0);

  srand(seed);

//*simple way:
  int rand_num[20];
  for (int i = 0; i < 21; i++) {
    rand_num[i] = (rand() % 8) + 1;
  }

  cout << "sizeof rand_num is: " << sizeof(rand_num) << " bytes" << endl;
  cout << "the rand nums:\n";
  int sum_of_rand_nums = 0;

  for (int i = 0; i < 20; i++) {
    cout << "index: " << i << " is: " << rand_num[i] << "\n";
    sum_of_rand_nums += rand_num[i];
    if (i == 19) {
      cout << "the sum of those nums is: " << sum_of_rand_nums << endl;
    }
  }
//accepted way
  cout << "HOW THE CLASS IS DOING IT:\n";

  int rand_num_1 = rand() % 8 + 1;
  int rand_num_2 = rand() % 8 + 1;
  int rand_num_3 = rand() % 8 + 1;
  int rand_num_4 = rand() % 8 + 1;
  int rand_num_5 = rand() % 8 + 1;
  int rand_num_6 = rand() % 8 + 1;
  int rand_num_7 = rand() % 8 + 1;
  int rand_num_8 = rand() % 8 + 1;
  int rand_num_9 = rand() % 8 + 1;
  int rand_num_10 = rand() % 8 + 1;
  int rand_num_11 = rand() % 8 + 1;
  int rand_num_12 = rand() % 8 + 1;
  int rand_num_13 = rand() % 8 + 1;
  int rand_num_14 = rand() % 8 + 1;
  int rand_num_15 = rand() % 8 + 1;
  int rand_num_16 = rand() % 8 + 1;
  int rand_num_17 = rand() % 8 + 1;
  int rand_num_18 = rand() % 8 + 1;
  int rand_num_19 = rand() % 8 + 1;
  int rand_num_20 = rand() % 8 + 1;


  cout << rand_num_1 << ", " << rand_num_2 << ", " << rand_num_3 << ", " << rand_num_4 << ", " << rand_num_5 << ", " << rand_num_6 << ", " << rand_num_7 << ", " << rand_num_8 << ", " << rand_num_9 << ", " << rand_num_10 << ", " << rand_num_11 << ", " << rand_num_12 << ", " << rand_num_13 << ", " << rand_num_14 << ", " << rand_num_15 << ", " << rand_num_16 << ", " << rand_num_17 << ", " << rand_num_18 << ", " << rand_num_19 << ", " << rand_num_20 << endl;
  cout << "The random num sum is: ";
  cout << rand_num_1 + rand_num_2 + rand_num_3 + rand_num_4 + rand_num_5 + rand_num_6 + rand_num_7 + rand_num_8 + rand_num_9 + rand_num_10 + rand_num_11 + rand_num_12 + rand_num_13 + rand_num_14 + rand_num_15 + rand_num_16 + rand_num_17 + rand_num_18 + rand_num_19 + rand_num_20 << endl;
  return 0;
}
