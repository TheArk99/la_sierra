/* Noah McMillan
 * Homework 2
 * Question 5
 * Program
 * October 9, 2023
 */

//Write a program that can be used as a math tutor for a young student. The program should display two 3-digit random numbers to be added, such as 247 + 129 = ?

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int check_user(){
  long user_guess = 0;
  while (user_guess > 8000 || user_guess < 100) {
    cin >> user_guess;
    if ( user_guess > 8000 || user_guess < 100){
      cout << "ENTER CORRECT INPUT\n";
    }
  }
  return user_guess;
}

int main(int argc, char **argv) {
  unsigned long seed = time(0);
  srand(seed);

  int rand_num_1 = (rand() % 899) + 100;
  int rand_num_2 = (rand() % 899) + 100;

  printf("What does %d + %d = ?: ", rand_num_1, rand_num_2);

  bool user_completion;

  while (user_completion != true){
    if (check_user() == rand_num_1 + rand_num_2){
      cout << "CORRECT!\n";
      user_completion = true;

    }
    else {
      cout << "INCORRECT!!!\nTry again?[y/n]: ";

      string yN;
      cin >> yN;

      if (yN == "y" || yN == "Y"){
          continue;
      }
      else if (yN == "n" || yN == "N"){
          user_completion = true;
      }
      else{
          continue;
      }
    }
  }

  return 0;
}
