/* Noah McMillan
 * Homework 2
 * Question 6
 * Program
 * October 9, 2023
 */

//Write a program to display the sin curve vertically using asterisks.
/*
  In order to get a smooth curve we’ll need to plot a lot of points. That’s a bit too much work for us for
now. So instead we’ll plot only 9 points in 45 degree increments starting at 0 and ending at 360
degrees for one cycle. Also, we’ll plot it vertically instead of the normal horizontal graph. Your
program will produce something like this
*/

#include <iostream>
#include <iomanip>
using namespace std;

const double pi = 3.1415926535897932385;

int main(int argc, char **argv) {

  float degrees[360]; //buffer 360 instead of 9 since i am doing that idk
  /*float degrees[9]; //buffer 360 instead of 9 since i am doing that idk
  degrees[0] = (0 * pi) / 180;
  degrees[1] = (45 * pi) / 180;
  degrees[2] = (90 * pi) / 180;//should equal 1.57075
  degrees[3] = (135 * pi) / 180;
  degrees[4] = (180 * pi) / 180;
  degrees[5] = (225 * pi) / 180;
  degrees[6] = (270 * pi) / 180;
  degrees[7] = (315 * pi) / 180;
  degrees[8] = (360 * pi) / 180;
*/
  //getting all 360 deggres plotted
  for (int i = 0; i < 360; i++){
    degrees[i] = (i * pi) / 180;
  }

  cout.setf(ios::fixed);
  cout.setf(ios::showpoint);
  cout.precision(3);

//dont actually need
/*  cout << "cordinates: ";

  for (int i = 0; i < 9; i++){
    cout << fabs(sin(degrees[i]) * 7);
    if (i != 8){
      cout << ", ";
    }
  }

  cout << "\n";
*/

  for (int i = 0; i < 360; i++){
//  for (int i = 0; i < 9; i++){
    cout << setw(fabs(sin(degrees[i]) * 50)) << "*\n"; //getting value of sind(degrees[i]) then gets abs value of the float, then mult by 7 to get larger distance (mult by 50 to show diff between all 360 points)
    //cout << setw(fabs(sin(degrees[i]) * 7)) << "*\n";
  }
  return 0;
}
