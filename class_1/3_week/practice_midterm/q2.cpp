/* Noah McMillan
 * midterm practice
 * Question 2
 * Program
 * October 12, 2023
 */

/*
Write a complete program to generate a random number between 1 and 10 inclusive. If
the number is greater than 8 then print out "you win." If the number is greater than 4 then
print out "almost." If it is less than or equal to 4 then print out "try again."
*/

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
using namespace std;
int main(int argc, char **argv) {
  unsigned int seed = time(0);
  srand(seed);

  unsigned int rand_num = (rand() % 10) + 1;
  printf("the num is: %d\n", rand_num);

  if (rand_num > 8){
    cout << "you win.\n";
  }
  else if (rand_num > 4){
    cout << "almost\n";
  }
  else if (rand_num <= 4){
    cout << "try again.\n";
  }


  return 0;
}
