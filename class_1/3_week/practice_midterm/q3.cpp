/* Noah McMillan
 * midterm practice
 * Question 3
 * Program
 * October 12, 2023
 */

/*
Write a program to enter the number of quarters, dimes, nickels, and pennies. Then print
out the total amount from all the coins.
*/

#include <iostream>
using namespace std;
int main(int argc, char **argv) {
  unsigned int quarters, dimes, nickels, pennies;

  cout << "quarters: ";
  cin >> quarters;
  cout << "dimes: ";
  cin >> dimes;
  cout << "nickels: ";
  cin >> nickels;
  cout << "pennies: ";
  cin >> pennies;

  cout << "total: " << quarters + dimes + nickels + pennies << endl;

  return 0;
}
