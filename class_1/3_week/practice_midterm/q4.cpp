/* Noah McMillan
 * midterm practice
 * Question 4
 * Program
 * October 12, 2023
 */

/*
Write a program to enter a number that represents the number of cents. Print out how
many quarters, dimes, nickels and pennies are needed to add up to that amount using the
least number of coins.
*/

#include <iostream>
using namespace std;


const unsigned int QUARTERS = 25;
const unsigned int DIMES = 10;
const unsigned int NICKELS = 5;
const unsigned int PENNIES = 1;


//#define QUARTERS 25
//#define DIMES 10
//#define NICKELS 5
//#define PENNIES 1

int main(int argc, char **argv) {
  unsigned int howManyCents;

  cout << "how many cents: ";
  cin >> howManyCents;

  unsigned int num_quarters = howManyCents / QUARTERS;
  if (num_quarters >= 1){
    howManyCents = howManyCents - (num_quarters * QUARTERS);
    cout << "Quarters: " << num_quarters;
  }

  unsigned int num_dimes = howManyCents / DIMES;
  if (num_dimes >= 1){
    howManyCents = howManyCents - (num_dimes * DIMES);
    cout << "\ndimes: " << num_dimes;
  }

  unsigned int num_nickels = howManyCents / NICKELS;
  if (num_nickels >= 1){
    howManyCents = howManyCents - (num_nickels * NICKELS);
    cout << "\nnickels: " << num_nickels;
  }

  unsigned int num_pennies = howManyCents / PENNIES;
  if (num_pennies >= 1){
    howManyCents = howManyCents - (num_nickels * PENNIES);
    cout << "\nPennies: " << num_pennies;
  }

  cout << "\n";

  return 0;
}

/*

//seeing what chatgpt outputed
#include <iostream>

int main() {
    int cents;
    std::cout << "Enter the number of cents: ";
    std::cin >> cents;

    int quarters, dimes, nickels, pennies;

    // Calculate the number of quarters
    quarters = cents / 25;
    cents %= 25;
    std::cout << "here is cents after %= " << 100 % 25 << std::endl;

    // Calculate the number of dimes
    dimes = cents / 10;
    cents %= 10;

    // Calculate the number of nickels
    nickels = cents / 5;
    cents %= 5;

    // The remaining cents will be in pennies
    pennies = cents;

    // Output the results
    std::cout << "Quarters: " << quarters << std::endl;
    std::cout << "Dimes: " << dimes << std::endl;
    std::cout << "Nickels: " << nickels << std::endl;
    std::cout << "Pennies: " << pennies << std::endl;

    return 0;
}
*/
