#include <iostream>
//#include <ctime>
#include <time.h>
//#include <cstdlib>
#include <stdlib.h>
using namespace std;

int main(){
  unsigned int seed = time(0);

  srand(seed); //initialization of rand

  unsigned int chose_sign = rand() % 4;
  int rand_num1 = rand() % 1000;
  int rand_num2 = rand() % 1000;

  int answer;
  int user_in;

  if (chose_sign == 0){
    cout << "what is " << rand_num1 << " + " << rand_num2 << ": ";
    answer = rand_num1 + rand_num2;
  }
  else if (chose_sign == 1){
    cout << "what is " << rand_num1 << " - " << rand_num2 << ": ";
    answer = rand_num1 - rand_num2;
  }
  else if (chose_sign == 2){
    cout << "what is " << rand_num1 << " * " << rand_num2 << ": ";
    answer = rand_num1 * rand_num2;
  }
  else {
    cout << "what is " << rand_num1 << " / " << rand_num2 << ": ";
    answer = rand_num1 / rand_num2;
  }

  cin >> user_in;

  if (user_in == answer){
    cout << "Correct!\n";
  }
  else {
    cout << "Incorrect!\n";
  }

  return 0;
}
