#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main(){
  unsigned int seed = time(0);
  srand(seed);
  int rand_num = rand() % 10 + 1;
  if (rand_num == 10){
    cout << "you win";
  }
  else if (rand_num >= 5 && rand_num <= 9){
    cout << "almost";
  }
  else {
    cout << "try again";
  }
}
