#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main(){
  unsigned int seed = time(0);
  srand(seed);
  unsigned int nums_biger_3 = 0;
  int nums_small_3 = 0;
  int rand_num;
  for (int i = 0; i < 4; i++){
    rand_num = 0;
    rand_num = rand() % 6 + 1;

    if (rand_num > 3){
      nums_biger_3 += rand_num;
    }
    else{
      nums_small_3 += rand_num;
    }
    cout << "nums bigger: " << nums_biger_3 << endl;
    cout << "nums smaller: " <<  nums_small_3 << endl;
  }
}
