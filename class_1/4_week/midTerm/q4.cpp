#include <iostream>
using namespace std;

int main(){
  int num;
  int num_even = 0;

  for (int i = 0; i < 4; i++){
    cout << "enter num to check if even: ";
    cin >> num;

    if (num % 2 == 0){
      cout << num << " is even\n";
      num_even++;
    }
  }
  cout << "num of even: " << num_even << endl;
}
