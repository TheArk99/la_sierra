/*
Write a program to print out your name 10 times on one line. You have to use the for
loop for this.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 0; i < 10; i++){
    cout << "Noah ";
  }
    cout << "\n";
}
