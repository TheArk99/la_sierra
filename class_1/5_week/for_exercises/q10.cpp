/*
Write a program to input 100 numbers, and then print out these 100 numbers
AFTER you have entered all of the 100 numbers.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  /*//one way with arrs
  int user_nums[100];
  for (int i = 0; i < 100; i++){
    printf("Enter %d num out of 100: ", i + 1);
    cin >> user_nums[i];
  }
  printf("Here are your 100 nums: \n");
  for (int i = 0; i < 100; i++){
    cout << user_nums[i] << endl;
  }
  */
  //c string through char *
  char user_nums[100];
  for (int i = 0; i < 100; i++){
    printf("Enter %d num out of 100: ", i + 1);
    cin >> user_nums[i];
  }
  printf("Here are your 100 nums: \n");
  cout << user_nums << endl;
}
