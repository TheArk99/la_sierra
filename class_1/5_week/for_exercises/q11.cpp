/*
Write a program to input 100 numbers. Have the program count how many of those
numbers are less than 50. Print out the count at the end.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int user_num;
  int count = 0;
  for (int i = 0; i < 100; i++){
    cout << "Enter a num: ";
    cin >> user_num;
    if (user_num < 50){
      count++;
    }
  }
  printf("The number of nums less than 50 is: %d\n", count);
}
