/*
Write a program to input a number into a variable n, and then print out n number of
asterisks (*). For example, if the user enters a 7, then the program will print out a row of
7 asterisks
*/


#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int user_num;
  cout << "Enter number of elements to print out: ";
  cin >> user_num;
  for (int i = 0; i < user_num; i++){
    cout << "* ";
  }
  cout << "\n";
}
