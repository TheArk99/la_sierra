/*
Write a program to input a number into a variable n and a symbol, and then print out n
number of that symbol For example, if the user enters a 7 and the symbol #, then the
program will print out a row of 7 #s like this:
#######
*/


#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int user_num;
  char user_char;
  cout << "Enter how many elements of the char you want print out: ";
  cin >> user_num;
  cout << "Enter how char you want printed out: ";
  cin >> user_char;

  for (int i = 0; i < user_num; i++){
    printf("%c", user_char);
  }
  cout << "\n";
}
