/*
* Write a program to input two numbers into two variables named column and row.
Print out a rectangle using asterisks (*) having column columns and row rows. Hint: you
need to use two FOR loops, one nested inside another. For example, if the user enters 5
for column and 2 for row, then the print out will be:
*****
*****
*/


#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int row, collumn;
  cout << "Enter how many rows to print: ";
  cin >> row;
  cout << "Enter how many collumns to print: ";
  cin >> collumn;
  for (int i = 0; i < row; i++){
    cout << "* ";
    for (int j = 0; j < collumn - 1; j++){
      cout << "* ";
    }
    cout << "\n";
  }
}
