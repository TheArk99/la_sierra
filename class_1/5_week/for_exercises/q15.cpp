/*
Enter a number and a symbol, and then print out a triangle with that number of rows
using that symbol. For example, if the user enters a 6 and the symbol # then the program
will print out
#
##
###
###
##
#
If the number entered is an odd number then the triangle will have one extra row in the
middle like the following with 7 rows
#
##
###
####
###
##
#
Your program needs to handle even/odd rows correctly. Also, if the user enters a negative
number, then the program will print out an error message. Hint: you need to use nested
FOR loops for this.
*/


#include <iostream>
using namespace std;

int main(){
  int user_num;
  char user_char;

  cout << "Enter number: ";
  cin >> user_num;
  cout << "Enter character: ";
  cin >> user_char;

  if (user_num < 0){
    cout << "\nERROR!!\n";
    return -1;
  }

    for (int i = 0; i < (user_num + 1) / 2; i++){
      for (int j = 0; j < i; j++){
        cout << user_char;
      }
        cout << user_char << endl;
    }
    for (int i = user_num / 2; i > 0; i--){
      for (int j = 1; j < i; j++){
        cout << user_char;
      }
        cout << user_char << endl;
    }

  return 0;
}
