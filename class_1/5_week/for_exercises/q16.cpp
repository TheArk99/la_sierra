/*
Write a program to automatically generate the first 40 Fibonacci numbers. Look up
Fibonacci numbers in Wikipedia. Hint: the best solution only needs 2 variables for
generating the sequence and 1 variable for counting to 40.
*/

#include <iostream>
using namespace std;

int main(){
  unsigned long fib = 1;
//unsigned long *pfib = &fib;
  unsigned long pre_fib = 0;
  for (int i = 0; i < 40; i++){
    fib += pre_fib;
    pre_fib = fib - pre_fib;
    cout << fib << endl;
  }
}
