/*
Write a program to print out your name 1000 times on separate lines. You have to use the
for loop for this.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 0; i < 1000; i++){
    cout << "Noah\n";
  }
}
