/*
Write a program to print out your name, address, and phone numbers 10 times on
separate lines. You have to use the for loop for this. The name, address and phone
numbers are also on separate lines like this
Dr. Hwang
4500 Riverwalk Parkway
Riverside, CA 92515
951-785-2054
*/

#include <iostream>
using namespace std;

struct Person {
  string name;
  string address;
  string city;
  string phone;
};

int main(){
  struct Person person;
  person.name = "noah";
  person.address = "28226 kane ct";
  person.city = "Highland, CA 92346";
  person.phone = "909-312-1431";

  for (int i = 0; i < 10; i++){
    cout << person.name << endl;
    cout << person.address << endl;
    cout << person.city << endl;
    cout << person.phone << endl;
    cout << "\n";
  }

}
