/*
Write a program to count from 1 to 100, i.e., print out the numbers from 1 to 100.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 1; i <= 100; i++){
    cout << i << endl;
  }
}
