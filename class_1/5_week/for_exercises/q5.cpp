/*
Write a program to print out the even numbers from 1 to 100.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 1; i < 101; i++){
    if (i % 2 == 0){
      cout << i << endl;
    }
  }
}
