/*
Write a program to print out the multiples of 7 from 1 to 100.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 0; i < 100; i++){
    cout << 7 * (i + 1) << endl;
  }
}
