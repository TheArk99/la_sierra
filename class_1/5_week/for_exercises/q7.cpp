/*
Write a program to sum all the numbers from 1 to 10,000.
*/

#include <iostream>
using namespace std;

int main(){
  int sum = 0;
  for (int i = 0; i < 10000; i++){
    sum += (i + 1);
  }
  cout << sum << endl;
}
