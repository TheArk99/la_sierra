/*
Write a program to enter one number and then print out the multiplication table from 1 to
10 for that one number. For example, if you enter 3, then print out 1 x 3 = 3, 2 x 3 = 6,
etc.
*/

#include <iostream>
using namespace std;

int main(){
  int user_num;
  cout << "Enter number: ";
  cin >> user_num;
  for (int i = 0; i < 10; i++){
    cout << user_num << " x " << i + 1 << " = " << user_num * (i+1) << ", ";
  }
  cout << endl;
}
