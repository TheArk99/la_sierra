/*
Write a program to input 100 numbers, and print out these 100 numbers anytime during
the execution of the program.
*/


#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int user_num;
  for (int i =0; i < 100; i++){
    cout << "Enter a num: ";
    cin >> user_num;
    printf("Here is %d of 100 nums that you entered: %d\n", i+1, user_num);
  }
}
