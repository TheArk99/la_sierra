/* Noah McMillan
 * for loop questions hw 4
 * Question 1
 * October 23, 2023
 */


/*
Write a program to print out your name 10 times
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 0; i < 10; i++){
    cout << "Noah ";
  }
    cout << "\n";
}
