/* Noah McMillan
 * for loop questions hw 4
 * Question 11
 * October 23, 2023
 */


/*
Write a program to input 10 numbers. The program will find and print out at the end the
largest and second largest number entered.
*/

#include <iostream>
using namespace std;

int main(){
  int user_nums[10];
  int GreatestNum = 0;
  int SecondGreatestNum = 0;
  for (int i = 0; i < 10; i++){
    cout << "Enter a number " << i + 1 << ": ";
    cin >> user_nums[i];
  }
  for (int i = 0; i < 10; i++){
    if (user_nums[i] > GreatestNum){
      SecondGreatestNum = GreatestNum;
      GreatestNum = user_nums[i];
    }
    else if (user_nums[i] > SecondGreatestNum && user_nums[i] != GreatestNum){
      SecondGreatestNum = user_nums[i];
    }
  }
  cout << "the largest num was: " << GreatestNum << endl;
  cout << "the second largest num was: " << SecondGreatestNum << endl;
}
