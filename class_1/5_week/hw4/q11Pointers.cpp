/* Noah McMillan
 * for loop questions hw 4
 * Question 11
 * October 23, 2023
 */


/*
Write a program to input 10 numbers. The program will find and print out at the end the
largest and second largest number entered.
*/
//for personal fun do this with pointers malloc/calloc and free or c++ equivalant

#include <iostream>
using namespace std;

int main(){
  int n;
  int * user_nums = NULL;
  int * pGreatestNum = NULL;
  int SecondGreatestNum = 0;

  cout << "Enter how many elements: ";
  cin >> n;

  user_nums = (int*)malloc(n * sizeof(int));

  if (user_nums == NULL){
    cout << "mem not allocated!!!\n";
    return -1;
  }

  for (int i = 0; i < n; i++){
    cout << "Enter a number " << i + 1 << ": ";
    cin >> user_nums[i];
  }

  pGreatestNum = &user_nums[0];

  for (int i = 0; i < n; i++){
    if (user_nums[i] > *pGreatestNum){
      SecondGreatestNum = *pGreatestNum;
      pGreatestNum = &user_nums[i];
    }
    else if (user_nums[i] > SecondGreatestNum && user_nums[i] != *pGreatestNum){
      SecondGreatestNum = user_nums[i];
    }
  }
  cout << "the largest num was: " << *pGreatestNum << endl;
  cout << "the second largest num was: " << SecondGreatestNum << endl;

  free(user_nums);
  return 0;
}
