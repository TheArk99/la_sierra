/* Noah McMillan
 * for loop questions hw 4
 * Question 2
 * October 23, 2023
 */

/*
Write a program to print out the numbers from 1 to 100.
*/

#include <iostream>
using namespace std;

int main(){
  for (int i = 0; i < 100; i++){
    cout << i + 1 << endl;
  }
}
