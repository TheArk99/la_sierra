/* Noah McMillan
 * for loop questions hw 4
 * Question 3
 * October 23, 2023
 */


/*
Ocean Levels. Assuming the ocean’s level is currently rising at about 1.5 millimeters per
year, write a program that displays a table showing the number of millimeters that the ocean
will have risen each year for the next 25 years.
*/

#include <iostream>
using namespace std;

//#define OCEAN_RISE 1.5

int main(){
  const float OCEAN_RISE = 1.5;
  float totalRise = 0;
  cout << "Year:\tRising:\n";
  for (int i = 0; i < 25; i++){
    totalRise = OCEAN_RISE * (i + 1);
    cout << i + 1 << "\t" << totalRise << endl;
  }

}
