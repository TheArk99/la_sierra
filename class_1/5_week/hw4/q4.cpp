/* Noah McMillan
 * for loop questions hw 4
 * Question 4
 * October 23, 2023
 */


/*
Calories Burned. Running on a particular treadmill you burn 3.6 calories per minute. Write a
program that uses a loop to display the number of calories burned after 5, 10, 15, 20, 25, and
30 minutes.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

//#define CALORIES_PER_MIN 3.6

int main(){
  const float CALORIES_PER_MIN = 3.6;
  float totalBurnt = 0;
  for (int i = 5; i <= 25; i = i + 5){
    totalBurnt = CALORIES_PER_MIN * i;
    printf("Caliories burnt after %d: %d", i, (int)totalBurnt);
    cout << "\n";
  }

}
