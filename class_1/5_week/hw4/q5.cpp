/* Noah McMillan
 * for loop questions hw 4
 * Question 5
 * October 23, 2023
 */


/*
Write a program that asks the user to enter today’s sales for five stores. The program should
then display a bar graph comparing each store’s sales. Create each bar in the bar graph by
displaying a row of asterisks. Each asterisk should represent $100 of sales. Here is an
example of the program’s output.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int storeSales[5];
  string charts[5];

  for (int i = 0; i < 5; i++){
    printf("Enter toady's sales for store %d: ", i+1);
    cin >> storeSales[i];
  }

  for (int i = 0; i < (sizeof(storeSales)/sizeof(int)); i++){
    charts[i] = "*";
    for (int j = 1; j < (storeSales[i] / 100); j++){
      charts[i] += "*";
    }
  }

  cout << "\nSALES BAR CHART\n(Each *= $100)\n";

  for (int i = 0; i < 5; i++){
     cout << "Store " << i + 1 << ": " << charts[i] << endl;
  }
  return 0;
}
