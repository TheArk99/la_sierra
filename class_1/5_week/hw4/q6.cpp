/* Noah McMillan
 * for loop questions hw 4
 * Question 6
 * October 23, 2023
 */


/*
Dollars for Pay. Write a program that calculates how much a person would earn in 30 days if
his or her salary is $10,000 each day. Use a for loop and not just a multiplication to find the
answer. Print out the dollar amount.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  const int salary = 10000;
  int totalIncome = 0;

  for (int i = 0; i < 30; i++){
    totalIncome += salary;
  }
  cout << "The total income was: " << totalIncome << endl;

  return 0;
}
