/* Noah McMillan
 * for loop questions hw 4
 * Question 7
 * October 23, 2023
 */


/*
Pennies for Pay. Write a program that calculates how much a person would earn in 30 days
if his or her salary is one penny the first day and two pennies the second day, and continues
to double each day. Display a table showing how much the salary is for each day, and then
show the total pay at the end. The output should be displayed in a dollar amount, not the
number of pennies.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int salary = 1;
  float totalIncome = 0;

  cout << "Day:\tSalary:\n";

  for (int i = 0; i < 30; i++){
    salary += i;
//    printf("Salary for day %d is: %d\n", i+1, salary);
    totalIncome += salary;
    printf("%d\t%d\n", i+1, salary);
  }
  totalIncome /= 100;
  cout << "--------------------------------------------\n";
  cout << "The total income was: $" << totalIncome << endl;

  return 0;
}
