/* Noah McMillan
 * for loop questions hw 4
 * Question 8
 * October 23, 2023
 */


/*
Dollars or Pennies for Pay? Write a program that will print out which option (Dollars for
Pay from #6 or Pennies for Pay from #7) is the better deal. Your program must combine
questions #6 and #7 together into one program, and based on the final amounts from the two,
decide and print out which is the better option.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int question6(){
//q6

  const int salary = 10000;
  int totalIncome = 0;

  for (int i = 0; i < 30; i++){
    totalIncome += salary;
  }
  return totalIncome;
}

int question7(){
//q7

  int salary = 1;
  float totalIncome = 0;


  for (int i = 0; i < 30; i++){
    salary += i;
    totalIncome += salary;
  //  printf("%d\t%d\n", i+1, salary);
  }
  totalIncome /= 100;
  return totalIncome;
}

int main(){

  if ( question7() > question6() ){
    cout << "The total income was from question 7: $" << question7() << endl;
  }
  else{
    cout << "The total income was from question 6: $" << question6() << endl;
  }


  return 0;
}
