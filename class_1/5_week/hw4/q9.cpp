/* Noah McMillan
 * for loop questions hw 4
 * Question 9
 * October 23, 2023
 */


/*
Write a program to input 10 numbers. The program will find and print out at the end the
largest number entered.
*/

#include <iostream>
using namespace std;

int main(){
  int user_nums[10];
  //copy var
//  int greatest_num = 0;
  //pointer var
  int * pGreatest_num = &user_nums[0];
  for (int i = 0; i < 10; i++){
    cout << "Enter a number " << i + 1 << ": ";
    cin >> user_nums[i];
  }
  for (int i = 0; i < 10; i++){
    if (user_nums[i] > *pGreatest_num){
      pGreatest_num = &user_nums[i];
    }
  }
  cout << "the largest num was: " << *pGreatest_num << endl;
}
