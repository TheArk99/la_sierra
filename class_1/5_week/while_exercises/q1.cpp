/*
Write a program to print out your name 10 times on one line. You have to use the for
loop for this.
*/

#include <iostream>
using namespace std;

int main(){
  int count = 1;
  do {
    cout << count << endl;
    count++;
  }while (count < 11);
}
