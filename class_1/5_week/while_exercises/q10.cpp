/*
Would you prefer to get $10,000 a day for 30 days or the payout as described in question
9? Write a program to determine which is the better option.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int dollars(){

  const int salary = 10000;
  int totalIncome = 0;
  int i = 0;

  while (i < 30){
    totalIncome += salary;
    i++;
  }
  return totalIncome;
}

int pennies(){

  int salary = 1;
  float totalIncome = 0;
  int i = 0;

  while (i < 30){
    salary += i;
    totalIncome += salary;
    i++;
  }
  totalIncome /= 100;
  return totalIncome;
}

int main(){

  cout << "The total income was from dollars salary: $" << dollars() << endl;
  cout << "The total income was from pennies salary: $" << pennies() << endl;

  if ( dollars() > pennies() ){
    cout << "the better option is 10,00 a day\n";
  }
  else{
    cout << "the better option is double pennies a day\n";
  }


  return 0;
}
