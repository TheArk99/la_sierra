/*
The Number Guessing Game – version 1 Player guesses. Write a program for the
computer to pick a random number between 0 and 99. The program will then repeatedly
ask the user to guess the number. Keep repeating until the guess is correct.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
using namespace std;

int main(){
  int user_guess;
  int rand_num;
  bool guessStatus = false;

  srand(time(0));
  rand_num = rand() % 100;


  while (guessStatus == false){
//    cout << rand_num; //debuging && testing
    cout << "Enter your guess: ";
    cin >> user_guess;
    if (user_guess == rand_num){
      cout << "You guess correctly!\n";
      guessStatus = true;
      break;
    }
  }
}
