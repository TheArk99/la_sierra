/*
The Number Guessing Game – version 2 Computer guesses. First, you as the user,
pick a number between 0 and 99. The computer will then repeatedly make a guess by
printing it out. After each guess, the user will enter whether the guess is correct or not.
Keep repeating until the guess is correct. At the end, print out how many times the
computer needs to guess the number.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
using namespace std;

int main(){
  int user_num;
  string user_answer;
  int comp_guess;
  bool guessStatus = false;
  int prev_guesses[100];
  int i = 1;

  srand(time(0));

  cout << "Enter num 0-99 for the computer to guess: ";
  cin >> user_num;

  while (guessStatus == false){
    comp_guess = rand() % 100;
    while (comp_guess == prev_guesses[i-1]){
      comp_guess = rand() % 100;
    }
    printf("Is the number %d this the right number? [yn]", comp_guess);
    cin >> user_answer;
    if (user_answer == "y" || user_answer == "Y"){
      cout << "\nNumber of guesses: " << i << endl;
      guessStatus = true;
      break;
    }
    prev_guesses[i-1] = comp_guess;
    i++;
  }
}
