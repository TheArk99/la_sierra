/*
The High-Low Number Guessing Game – version 3 Computer guesses. Similar to
question 12, but instead of the user just telling the computer whether the guess is correct
or not, the user can also tell the computer whether its guess is too high (i.e. the guess is
higher than the number) or too low (i.e. the guess is lower than the number). Based on
this, the computer should be able to guess the number in fewer tries then for the solution
for question 12.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
using namespace std;

int main(){
  int user_num;
  string user_answer;
  bool guessStatus = false;
  int min_max[2] = {0, 99};
  int i = 1;
  int range = 100;

  srand(time(0));
  int comp_guess = rand() % 100;

  cout << "Enter num 0-99 for the computer to guess: ";
  cin >> user_num;

  while (guessStatus == false){
    printf("Is the number %d this the right number? [yn]", comp_guess);
    cin >> user_answer;
    if (user_answer == "y" || user_answer == "Y"){
      cout << "\nNumber of guesses: " << i << endl;
      guessStatus = true;
      break;
    }
    else{
      string lh;
      cout << "Is the number to low or high [L/H]";
      cin >> lh;
      if (lh == "l" || lh == "L"){
        min_max[0] = comp_guess;
        comp_guess = (rand() % (min_max[1] - min_max[0])) + min_max[0] + 1;
      }
      else{
        min_max[1] = comp_guess;
        comp_guess = (rand() % (min_max[1] - min_max[0] )) + min_max[0];
      }
    }
    i++;
  }
  return 0;
}
