/*
Write a program to keep asking the user to enter a number until the number entered in is a
zero. The program terminates when the user enters a zero.
*/

#include <iostream>
using namespace std;

int main(){
  int user_num;
  do {
    cout << "Enter a number: ";
    cin >> user_num;
  }while (user_num != 0);
}
