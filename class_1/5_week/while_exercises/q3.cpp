/*
just like question 2 but in addition when the program terminates, it will print out how
many numbers the user has entered in.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int user_num;
  int i = 0;
  do {
    cout << "Enter a num: ";
    cin >> user_num;
    i++;
  }while (user_num != 0);
  printf("the num of times nums enetered: %d\n", i);
}
