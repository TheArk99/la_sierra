/*
Just like question 2 but in addition when the program terminates, it will print out the
average of all the numbers the user has entered in.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int user_num;
  int sum_of_nums = 0;
  int i = 0;
  do {
    cout << "Enter a num: ";
    cin >> user_num;
    sum_of_nums += user_num;
    i++;
  }while (user_num != 0);
  printf("average: %.2f\n", (float)sum_of_nums/i);
}
