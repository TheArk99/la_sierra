/*
Just like question 2 but in addition when the program terminates, it will print out how
many even numbers and how many odd numbers the user has entered in.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int user_num;
  int even = 0;
  int odd = 0;
  do {
    cout << "Enter a num: ";
    cin >> user_num;
    int is_even = user_num % 2;
    switch (is_even){
      case 0:
        even++;
        break;
      default:
        odd++;
    }
  }while (user_num != 0);
  printf("even: %d\nodd: %d\n", even, odd);
}
