/*
Write a program to enter two numbers, num1 and num2. num1 is divided by num2 and the
result is displayed. But before the division, num2 is tested for the value 0. If it is a 0, the
division does not take place and the program will keep asking the user to enter another
number until a non-zero number is entered for num2.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int user_num1;
  int user_num2;
  cout << "Enter num for num 1: ";
  cin >> user_num1;
  cout << "Enter num for num 2: ";
  cin >> user_num2;
  while (user_num2 == 0){
    cout << "Need non zero for num 2!!\nRENTER: ";
    cin >> user_num2;
  }
  printf("num1 / num2 is: %.2f\n", (float)user_num1 / user_num2);
}
