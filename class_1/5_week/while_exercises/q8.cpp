/*
Write a program to generate two random numbers between 0 and 100. Ask the user to
enter the answer for the sum of the two numbers. Print out whether the answer entered by
the user is correct or not. See the Random Number document on how to generate random
numbers. If the answer is wrong then keep repeating to ask the user to enter the correct
sum.
*/

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <stdio.h>
using namespace std;

int main(){
  srand(time(0));
  int rand_num1 = rand() % 101;
  int rand_num2 = rand() % 101;
  int user_answer;
  do {
    cout << "Guess what is the rand num: ";
    cin >> user_answer;
  } while (user_answer != rand_num1 + rand_num2);
  cout << "Correct!!\n";
}
