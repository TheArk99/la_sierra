/*
Write a program to determine how much a person would get if s/he is given one penny
the first day, two pennies the second day, four pennies the third day, and continues to
double each day until day 30. Display a table showing the day, how much for each day
and the accumulated sum up to that day for the 30 days. Finally show the total amount in
dollars at the end that the person would get.
*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  int salary = 1;
  float totalIncome = 0;
  int i = 0;

  cout << "Day:\tSalary:\n";
  while (i < 30){
    salary += i;
    totalIncome += salary;
    printf("%d\t%d\n", i+1, salary);
    i++;
  }
  totalIncome /= 100;
  cout << "--------------------------------------------\n";
  cout << "The total income was: $" << totalIncome << endl;

  return 0;
}
