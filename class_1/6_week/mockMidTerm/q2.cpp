#include <stdio.h>
#include <cstdlib>
#include <ctime>

int main(){
  srand(time(0));
  unsigned int rand_num = rand() % 100;
  unsigned int pre_rand_num;

  do{
    pre_rand_num = rand_num;
    rand_num = rand() % 100;
  }while ((pre_rand_num + rand_num) < 70);

  printf("The final adjacent nums were: %d, %d\n", pre_rand_num, rand_num);

  return 0;
}
