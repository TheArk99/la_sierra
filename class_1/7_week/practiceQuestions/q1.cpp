#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

using std::cout;
using std::endl;

int main(void){
  srand(time(0));
  unsigned long rand_num;

  do{
    rand_num = rand();
  } while (rand_num != 20);

  printf("rand_num == %lu\n", rand_num);
  return 0;
}
