#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

using std::cout;
using std::endl;

int main(void){
  srand(time(0));
  unsigned long rand_num;
  bool is_20 = false;
  bool is_40 = false;

  do{
    rand_num = rand();
    if (rand_num == 20){
      is_20 = true;
    }
    if (rand_num == 40){
      is_40 = true;
    }
  } while (is_20 == false && is_40 == false);


  return 0;
}
