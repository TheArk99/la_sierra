#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
using std::cout;
using std::endl;

int main(void){
  srand(time(0));
  unsigned long rand_num;
  bool is_12345 = false;
  bool is_54321 = false;
  int first_num_to_appear;
  do{
    rand_num = rand() % 999001;
    if (rand_num == 12345){
      is_12345 = true;
    }
    if (rand_num == 54321){
      is_54321 = true;
    }
    if (is_54321 == true && is_12345 == false){
      first_num_to_appear = 54321;
    }else if (is_54321 == false && is_12345 == true){
      first_num_to_appear = 12345;
    }
  }while(is_12345 == false && is_54321 == false);
  printf("first num to appear was: %d\n", first_num_to_appear);
  return 0;
}
