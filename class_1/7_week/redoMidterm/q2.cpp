#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
using std::cout;
using std::endl;

/*
Write a program that follows the steps below for estimating the value of π:
1) Initialize a variable called circle_points_count to 0.
2) Generate a random number x between -1 and 1 (see note below).
3) Generate a random number y between -1 and 1 (see note below).
4) Calculate the distance = x2 + y2
5) If distance ≤ 1 then increment circle_points_count.
6) Go back and repeat from step 2) 10000 times
7) Calculate Pi = 4.0 × circle_points_count / 10000.0
8) Print out the value of Pi.
Note: To get any meaningful estimation of Pi, we need to use floats instead of integers
when we generate the random numbers for x and y in steps 2) and 3). (Otherwise, you
only get the numbers -1, 0 and 1.) One way to do this is to generate random numbers
between 1 and 10000 and then divide it by 10000.
*/


int main(void){
  srand(time(0));
  //debuging
//  srand(3);
  unsigned int circle_points_count = 0;
  double y;
  double x;
  double dist;
  double pi;
  for (int i = 0; i < 10000; i++){
     x = (float)(rand() % 10000 + 1) / 10000;
     y = (float)(rand() % 10000 + 1) / 10000;
     dist = pow(x, 2) + pow(y, 2);
     if (dist <= 1){
       circle_points_count++;
    }
  }
  pi = 4.0 * circle_points_count / 10000.0;
  printf("pi is: %.3lf\n", pi);
  return 0;
}
