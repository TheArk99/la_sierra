#include <stdio.h>
#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;
using std::setw;

/*
Write a program to plot one cycle of the sine curve vertically using asterisks with 1000
points. One cycle is 360 degrees or 2π radians. This question is similar to the sine curve
question from one of your homework except that we are using 1000 points instead of 9.
Hint: You’ll need to use the sin function, and the setw command. The conversion formula
between radian and degree is
radian = degree * π/180
*/

int main(void){
  const double pi = 3.1415;
  double rad;
  for (int i = 0; i < 20; i++){
    rad = (i / 360) * pi / 180;
    cout << setw(sin(rad)) << "*\n";
  }
  return 0;
}
