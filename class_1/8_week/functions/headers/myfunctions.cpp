#include "myfunctions.h"

/****************************************************************
 * the sum function */
int sum(int x, int y){
return x + y;
}
/****************************************************************
 * the swap function */
void swap(int &x, int &y){
int temp;
 temp = x;
 x = y;
 y = temp;
}
/****************************************************************
 * the changeArray function */
void changeArray(int a[], int size){
for (int i = 0; i<size; i++) {
 a[i] = a[i]*2;
 }
}
