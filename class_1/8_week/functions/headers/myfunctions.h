#ifndef _MYFUNCS_
#define _MYFUNCS_

int sum(int x, int y);
void swap(int &x, int &y);
void changeArray(int a[], int arraySize);

#endif

