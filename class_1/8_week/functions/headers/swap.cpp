#include "swap.h"

void swap (int &x, int &y){
  int tempVar = x;
  x = y;
  y = tempVar;
}
