#include <stdio.h>
#include <iostream>
#include "headers/myfunctions.h"
using namespace std;

int main(void){
  int m=5, n=9;
  int a[5]={1,2,3,4,5};
  int s;
  int i;

   // sum function example
   s = sum(m,n);
  cout << "The sum of " <<m<< " and " <<n<< " is "<<s<< endl<<endl;

   // swap function example
  cout << "Before swapping m is " << m << " and n is " << n << endl;
  swap(m,n);
  cout << "After swapping m is " << m << " and n is " << n << endl;

   // changeArray function example
  cout << endl << "The original array is: ";
  for(i=0;i<5;i++) {
   cout << a[i] << " ";
   }

  changeArray(a,5);

  cout << endl << "The changed array is: ";
  for(i=0;i<5;i++) {
   cout << a[i] << " ";
   }
  return 0;
}
