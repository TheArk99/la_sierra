#include <stdio.h>
#include <iostream>
#include <math.h>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function that calculates the sin of two numbers. The two numbers are passed
into the function and the function returns two numbers which are the sin of these two
numbers. You can use the trig function sin(x). You need to #include <math.h>.
*/

//sine calculations

//pass by pointer
//void calcSine(float *x, float *y){
//  *x = sin(*x);
//  *y = sin(*y);
//}

//pass by refrence
void calcSine(float &x, float &y){
  x = sin(x);
  y = sin(y);
}

/* the main function */
int main (void) {
  float x = 1, y = 2;
  calcSine(x, y);
  printf("The sine of x is: %f\nThe sine of y is: %f\nThe Address of x is: %p\nThe address of y is: %p\n", x, y, &x, &y);
 return 0;
}
