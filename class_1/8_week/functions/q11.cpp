#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function that calculates the average of numbers in an array. The array is passed
into the function and the function returns the average.
*/

int calcAverage (int *arr, int arrSize){
  int sumOfArr = 0;
  for (int i = 0; i < arrSize; i++){
    sumOfArr += arr[i];
  }
  return (sumOfArr / arrSize);
}

/* the main function */
int main (void) {
  int arr[5] = { 1, 2, 3, 4, 5 };
  int arrSize = sizeof(arr)/sizeof(arr[0]);
  int average = calcAverage(arr, arrSize);
  printf("The average of the array is: %d\n", average);
 return 0;
}
