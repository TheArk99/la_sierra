#include <stdio.h>
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cin;
using std::string;

/*
Write a function to sort numbers in an array. The array is passed into the function and
the function returns the sorted array.
*/

//simple sort algo, buble sort, don't make it more complicated than it needs to be by doing something like a merge sort :)

//swap elements in arr
void swap(int &x, int &y){
  int temp = x;
  x = y;
  y = temp;
}

void sortArr(int *unsortedArr, int arrSize){
//void sortArr(string *unsortedArr, int arrSize){
  for (int i = 0; i < arrSize; i++){
    for (int j = 0; j < i; j++){
      if (unsortedArr[i] < unsortedArr[j]){
        swap(unsortedArr[i], unsortedArr[j]);
      }
    }
  }
}


/* the main function */
int main (void) {
  int unsortedArr[5] = { 3, 5, 1, 4, 2 };
//  string unsortedArr[5] = { "hello", "apple", "some apple", "care", "pear" };
  int arrSize = sizeof(unsortedArr) / sizeof(unsortedArr[0]);

  sortArr(unsortedArr, arrSize);

  for (int i = 0; i < arrSize; i++){
    cout << unsortedArr[i] << endl;
  }

  return 0;
}
