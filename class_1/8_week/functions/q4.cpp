#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Implement the following function:
void changes(int &x, int y){
 x = x * 2;
 y = y * 2;
}
In the main routine, type in:
int m=3, n=7;
cout << "Before calling changes, m is " << m << " and n is " << n
<< endl;
changes(m,n);
cout << "After calling changes, m is " << m << " and n is " << n
<< endl;
 Which variable got changed? Which variable did not change? Why?
*/

void changes(int &x, int y){
 x = x * 2;
 y = y * 2;
}
/* the main function */
int main (void) {
  int m=3, n=7;
  cout << "Before calling changes, m is " << m << " and n is " << n << endl;
  changes(m,n);
  cout << "After calling changes, m is " << m << " and n is " << n << endl;

 return 0;
}
