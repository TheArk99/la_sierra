#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function named timesTen. The function should have an integer parameter
named number. When timesTen is called, it should display the product of number
times 10. The function does not return anything.
*/

void timesTen(int &number){
  //for (int i = 0; i < 10; i++){
  //  number *= number;
  //}
  number *= 10;
}

/* the main function */
int main (void) {
  int number = 5;
  timesTen(number);
  cout << number << endl;
 return 0;
}
