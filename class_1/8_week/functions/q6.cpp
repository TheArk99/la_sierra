#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function named timesTen. The function should have an integer parameter
named number. When timesTen is called, it calculates the product of number times
10. The function returns this product.
*/

int timesTen(int number){
  //for (int i = 0; i < 10; i++){
  //  number *= number;
  //}
  number *= 10;
  return number;
}

/* the main function */
int main (void) {
  int number = 5;
  cout << timesTen(number) << endl;
 return 0;
}
