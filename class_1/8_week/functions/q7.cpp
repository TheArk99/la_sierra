#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function that prints out the larger of two numbers. The two numbers are passed
into the function. The function does not return anything.
*/

void determineLargest(int x, int y, int &largestNum){
  if (x < y){
    largestNum = y;
  }
  else if (x > y){
    largestNum = x;
  }
}

/* the main function */
int main (void) {
  int x = 5, y = 4;
  int largestNum = NULL;
  determineLargest(x, y, largestNum);
  printf("The largest number of the 2 was: %d\n", largestNum);
 return 0;
}
