#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function that determines the larger of two numbers. The two numbers are passed
into the function and the function returns the largest number.
*/

int determineLargest(int x, int y){
  if (x < y){
    return y;
  }
  else if (x > y){
    return x;
  }
  return 1;
}

/* the main function */
int main (void) {
  int x = 5, y = 5;
  int largestNum = determineLargest(x, y);
  if (largestNum == 1){
    fprintf(stderr, "error!!\n");
    return -1;
  }
  printf("The largest number of the 2 was: %d\n", largestNum);
 return 0;
}
