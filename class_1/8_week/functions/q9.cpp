#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a function that calculates the average of two numbers. The two numbers are passed
into the function and the function returns the average.
*/

int calcAverage(int &num1, int &num2){
  int average = (num1 + num2) / 2;
  return average;
}

/* the main function */
int main (void) {
  int num1 = 5, num2 = 10;
  int average = calcAverage(num1, num2);
  printf("The average is: %d\n", average);
 return 0;
}
