#include <stdio.h>
#include <iostream>
#include "headers/myfunctions.h"
using std::cout;
using std::endl;
using std::cin;

/*
Implement the three functions from section 1 above and make sure that they work.
*/
int sum (int x, int y) { // declaring the function
 int s;
 s = x + y;
 return s; // returning a value for the function
}
/* the main function */
int main (void) {
  int s;

  s = sum(5, 4); // calling the function
  cout << "The sum of " <<m<< " and " <<n<< " is "<<s<< endl;
  int var1 = 5, var2 = 3;
  swap(var1, var2);
  printf("After swap var1 is: %d\nAfter swap var2 is: %d\n", var1, var2);
 return 0;
}
#include <stdio.h>
#include <iostream>
#include "headers/myfunctions.h"
using namespace std;

int main(void){
  int m=5, n=9;
  int a[5]={1,2,3,4,5};
  int s;
  int i;

   // sum function example
   s = sum(m,n);
  cout << "The sum of " <<m<< " and " <<n<< " is "<<s<< endl<<endl;

   // swap function example
  cout << "Before swapping m is " << m << " and n is " << n << endl;
  swap(m,n);
  cout << "After swapping m is " << m << " and n is " << n << endl;

   // changeArray function example
  cout << endl << "The original array is: ";
  for(i=0;i<5;i++) {
   cout << a[i] << " ";
   }

  changeArray(a,5);

  cout << endl << "The changed array is: ";
  for(i=0;i<5;i++) {
   cout << a[i] << " ";
   }
  return 0;
}
