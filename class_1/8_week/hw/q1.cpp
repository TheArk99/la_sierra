#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main(void){
  int user_nums[10];
  for (int i = 0; i < 10; i++){
    printf("Enter num %d/10: ", i);
    cin >> user_nums[i];
  }
  cout << "The numbers you entered were: ";
  for (int j = 0; j < 10; j++){
    cout << user_nums[j] << " ";
  }
  return 0;
}
