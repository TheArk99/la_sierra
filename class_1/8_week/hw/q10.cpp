#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Create a two-dimensional 10×10 array and have the program automatically fill it with
the numbers for the 10×10 multiplication table. You need to use two for loops for this. To
check that the program is correct you will need to print out this two-dimensional array.
*/

int main(void){
  int arr[10][10];
  for (int i = 0; i < 10; i++){
    for (int j = 0; j < 10; j++){
      arr[i][j] = (i + 1) * (j + 1);
    }
  }
  for (int i = 0; i < 10; i++){
    for (int j = 0; j < 10; j++){
      printf("The array at dimension %d, at index %d was: %d\n", i, j, arr[i][j]);
    }
  }
  return 0;
}
