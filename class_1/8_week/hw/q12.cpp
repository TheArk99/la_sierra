#include <stdio.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
using std::cout;
using std::endl;
using std::cin;

/*
Write a program to generate 11 random numbers from 1 to 50. Store these 11 numbers
in an array. Now calculate and print out the median of these numbers.
*/

int main(void){
  srand(time(0));
  int randNums[11];
  unsigned int size = sizeof(randNums) / sizeof(randNums[0]);
  int median;
  for (int i = 0; i < 11; i++){
    randNums[i] = rand() % 50 + 1;
    printf("%d: %d\n", i, randNums[i]);
  }
  median = size / 2 + 1;
  printf("the median was: %d at index: %d\n", randNums[median], median);
  return 0;
}
