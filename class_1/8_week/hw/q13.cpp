#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a program to input an integer. Print out whether the number entered is a prime
number or not.
*/

int main(void){
  int user_num;
  bool isPrime;
  cout << "Enter a num to see if it is a prime num: ";
  cin >> user_num;
  if (user_num == 1){
    isPrime = false;
  }
  else if (user_num == 2 || user_num == 3){
    isPrime = true;
  }
  else if (user_num % 2 == 0 || user_num % 3 == 0){
    isPrime = false;
  }
  else{
    isPrime = true;
  }
  if (isPrime == true){
    cout << "Entered num is a prime\n";
  }
  else{
    cout << "Entered num is not prime\n";
  }

  return 0;
}
