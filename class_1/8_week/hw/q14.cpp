#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Use the Sieve of Eratosthenes algorithm for finding all the prime numbers from 2 to
500. The algorithm:

1. Create a list of consecutive integers from two to n: (2, 3, 4, ..., n).
2. Initially, let p equal 2, the first prime number.
3. Cross out from the list all multiples of p greater than p.
4. Find the first number remaining in the list greater than p (this number is the next prime); let p equal this number.
5. Repeat steps 3 and 4 until p^2 is greater than n.
6. All of the remaining numbers in the list are prime.

Hint: For step 1, you will need to use an array to keep track of whether a number has
been crossed out or not. The indexes for the array are the integers. Initialize the array
with 0’s. A 0 in a location means that that number is not crossed out, and a 1 in that
location means that that number has been crossed out. For step 3, you can cross out a
number simply by changing the content of that number index location to a 1.
*/

int main(void){
  int p = 2;
  int n = 500;
  int consecutiveInts[n];
  for (int i = 0; i < n - 1; i++){
    consecutiveInts[i] = i + 2;
  }
  for (int i = 0; i < n; i++){
    if (pow(p, 2) > n){
      break;
    }
  }
  return 0;
}
