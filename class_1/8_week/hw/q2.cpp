#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;

int main(void){
  int nums_to_calc[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  int sum = 0;
  for (int i = 0; i < 10; i++){
    sum += nums_to_calc[i];
  }
  printf("The sum of the 10 elements was: %d", sum);
  return 0;
}
