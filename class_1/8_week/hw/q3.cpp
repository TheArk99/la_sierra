#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;

int main(void){
  int nums_to_calc[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  int arr_elements_added_together = 0;
  float average;

  int elements_in_arr = sizeof(nums_to_calc)/sizeof(nums_to_calc[0]);

  for (int i = 0; i < 10; i++){
    arr_elements_added_together += nums_to_calc[i];
  }

  average = (float)arr_elements_added_together / elements_in_arr;

  printf("The average of the 10 elements is : %.2f\n", average);

  return 0;
}
