#include <stdio.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
using std::cout;
using std::endl;

int main(void){
  srand(time(0));
  int rand_nums[10];
  int largestNum;
  for(int i = 0; i < 10; i++){
    rand_nums[i] = rand() % 50 + 1;
  }
  largestNum = rand_nums[0];
  for (int i = 1; i < 10; i++){
    if (rand_nums[i] > largestNum){
      largestNum = rand_nums[i];
    }
  }
  printf("The largest num was: %d", largestNum);
  return 0;
}
