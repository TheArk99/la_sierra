#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
using std::cout;
using std::endl;

int main(void){
  srand(time(0));
  int randNums[10];
  int largestNum;
  int secondLargestNum;
  for (int i = 0; i < 10; i++){
    randNums[i] = rand() % 50 + 1;
    printf("%d, ", randNums[i]);
  }
  if (randNums[0] > randNums[1]){
    largestNum = randNums[0];
    secondLargestNum = randNums[1];
  }
  else{
    largestNum = randNums[1];
    secondLargestNum = randNums[0];
  }

  for (int i = 2; i < 10; i++){
    if (randNums[i] > largestNum){
      secondLargestNum = largestNum;
      largestNum = randNums[i];
    }
  }

  printf("The largestNum was: %d\nThe secondLargestNum was: %d\n", largestNum, secondLargestNum);

  return 0;
}
