#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Write a program to enter an integer number and then generate the multiplication table
from 1 to 10 for that one number. Put these numbers into an array of size 10. After that,
print out the table from the array
*/

int main(void){
  int user_num;
  int multiplacationTable[10];
  cout << "Enter a num to have the multipliation table: ";
  cin >> user_num;
  for (int i = 0; i < 10; i++){
    multiplacationTable[i] = (i+1) * user_num;
  }
  printf("The multiplacation table is:\n");

  for (int j = 0; j < 10; j++){
    printf("%d\n", multiplacationTable[j]);
  }

  return 0;
}
