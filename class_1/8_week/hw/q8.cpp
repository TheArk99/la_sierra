#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/*
Repeat question 7 but also print out the message “number NOT found” if the number is
not found in the array.

question 7:
Write a program with an array of size 10. Initialize the array with numbers of your
choice. Have the user enter a number, and then search for this number in the array. Print
out the message “number found” if the number entered is found in the array.

*/

int main(void){
  int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 8, 10 };
  const unsigned int SIZE = sizeof(arr)/sizeof(arr[0]);
//  int median = SIZE / 2;
  bool found = false;
  int user_num;
  cout << "Enter num to find in arr: ";
  cin >> user_num;
  for (int i = 0; i < SIZE; i++){
/*    if (arr[] == user_num){
      break;
    }
    else if (arr[] < user_num){
    }
    else if (arr[] > user_num){
    }*/
    if ( arr[i] == user_num ){
      found = true;
      break;
    }
  }

  if (found == false){
    cout << "number not found\n";
  }
  else if (found == true){
    cout << "Number found\n";
  }

  return 0;
}
