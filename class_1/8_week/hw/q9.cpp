#include <stdio.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::string;

/*
Write a program to print out a message depending on an input number. If the input
number is 1 then print out the message “Good morning”, if the number is 2 then print out
the message “Good afternoon”, if the number is 3 then print out the message “Good
evening”, if the number is 4 then print out the message “Good night” and if the number is
anything else then print out the message “Invalid input.” The program repeats if the input
number is invalid.
a) Use nested if statements to do this.
b) Use a switch statement to do this.
c) Use parallel arrays to do this.
*/

int main(void){
  int userNum;
  string message[4] = { "Good morning", "Good afternoon", "Good evening", "Good night" };

  cout << "Enter input num: ";
  cin >> userNum;

  switch (userNum){
    case 1:
      printf("The message was: %s\n", message[0].c_str());
      break;
    case 2:
      printf("The message was: %s\n", message[1].c_str());
      break;
    case 3:
      printf("The message was: %s\n", message[2].c_str());
      break;
    case 4:
      printf("The message was: %s\n", message[3].c_str());
      break;
    default:
      printf("Invalid input\n");
      return 1;
  }

  //options a if statments
  /*
  if (userNum == 1){
      printf("The message was: %s\n", message[0].c_str());
  }
  else if (userNum == 2){
      printf("The message was: %s\n", message[1].c_str());
  }
  else if (userNum == 3){
      printf("The message was: %s\n", message[2].c_str());
  }
  else if (userNum == 4){
      printf("The message was: %s\n", message[3].c_str());
  }
  else{
      printf("Invalid input\n");
  }
  //option c parallel array
  int size = 4;
  int checkNum[size] = { 1, 2, 3, 4 };
  string message[size] = { "Good morning", "Good afternoon", "Good evening", "Good night" };

  */

  return 0;
}
