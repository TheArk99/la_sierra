#include <stdio.h>
#include <iostream>


int main(){
  int arr[10];
  for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++){
    std::cout << "Enter 1 out of " << i + 1 << " numbers: ";
    std::cin >> arr[i];
  }
  std::cout << "Here are the nums: " << std::endl;
  for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++){
    std::cout << arr[i]<<std::endl;
  }
  return 0;
}
