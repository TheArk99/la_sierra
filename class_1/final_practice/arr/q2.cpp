#include <stdio.h>
#include <iostream>
#include <ctime>

using namespace std;

void swap(int &x, int &y){
  int temp = x;
  x = y;
  y = temp;
}


void bubleSort(int arr[], int n){
  for (int i = 0; i < n; i++){
    for (int j = 0; j < i; j++){
      if (arr[i] < arr[j]){
        swap(arr[i], arr[j]);
      }
    }
  }
}

int main(){
  srand(time(0));
  int arr[11];
  int arrSize;
  int median = 0;
  for (int i = 0; i < 11; i++){
    arr[i] = rand() % 50;
  }
  arrSize = sizeof(arr) / sizeof(arr[0]);
  bubleSort(arr, arrSize);
  median = arr[arrSize / 2];

  for (int i = 0; i < 11; i++){
    cout << "Index " << i << " is: "<<arr[i]<<endl;
  }

  cout << "The median is: " << median<< endl;

  return 0;
}
