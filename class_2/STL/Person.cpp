#include "Person.h"


Person::Person(){
  name = "";
  age = 0;
}

Person::Person(string s, int i){
  name = s;
  age = i;
}

Person::Person(int cS, int i){
  char s = cS;
  name = s;
  age = i;
}

Person::Person(char s, int i){
  name = s;
  age = i;
}

ostream& operator<<(ostream& out, const Person& p){
  out << p.name << " " << p.age;
  return out;
}

istream& operator>>(istream& in, Person& p){
  in >> p.name >> p.age;
  return in;
}

bool Person::operator<(const Person &a) const{
  Person p1 = *this;
  Person p2 = a;
  return p1.age < p2.age;
}

bool Person::operator>(const Person &a) const{
  Person p1 = *this;
  Person p2 = a;
  return p1.age > p2.age;
}
