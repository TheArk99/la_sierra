#ifndef __STLCHALLENGE__
#define __STLCHALLENGE__

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

class Person {
  private:
    string name;
    int age;
  public:
    Person();
    Person(string s, int i);
    Person(char s, int i);
    Person(int cS, int i);
    friend ostream& operator<<(ostream& out, const Person& p);
    friend istream& operator>>(istream& in, Person& p);
    bool operator<(const Person &a) const;
    bool operator>(const Person &a) const;
};
#endif

