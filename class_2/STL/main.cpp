#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#include <ctime>
#include "Person.h"
using namespace std;

/*
Create a Person class with the two data members:
2. Overload only the necessary member functions for the class.
3. Check if a random file name mydata.dat exists or not. If the file doesn’t exists then:
a. Add 20 Person records into a STL vector. Initialize the records with random data.
b. Use the STL sort algorithm to sort the vector. Hint. This might require overloading the
needed Person operator.
c. Print out all the records in the vector to verify that they are sorted. Hint. This might require
overloading the needed Person operator.
d. Write all the records in the vector to the random file mydata.dat.
If the file exists then:
e. Read all the Person records from the random file mydata.dat and add them to a STL vector.
f. Print out all the records from the vector. Hint. This might require overloading the needed
Person operator.
Assuming that initially the file mydata.dat does not exist, so the first time you run the program, it will
execute steps 3a. to 3d. Subsequent runs of the program will execute steps 3e. to 3f.
*/

void readFile(fstream &file, int &file_location, vector<Person> &data);

int main(int argv, char** argc){
  const string FILE_NAME = "mydata.dat";
  bool fileExists = false;
  int file_location = 0;
  fstream file;
  srand(time(0));

  /*
  if (argv >= 2){
    if (argv > 2){
    }
  }else{
  }
  */

  vector<Person> data;

  file.open(FILE_NAME, ios::in);

  if (file.is_open()) {
    fileExists = true;
    readFile(file, file_location, data);
    file.close();
  //  sort(data.begin(), data.end());
    for (int i = 0; i < 20; i++){
      cout << data[i] << endl;
    }
    return 0;
  }

  if (fileExists == false){
    file.close();
    file.open(FILE_NAME, ios::app | ios::in);
  }

  for (int i = 0; i < 20; i++){
    data.push_back(Person(rand() % ((122 - 65) + 1) + 65, rand() % 100));
  }

  sort(data.begin(), data.end());
  for (int i = 0; i < 20; i++){
    cout << data[i] << endl;
    file.seekp(file_location, ios::beg);
    file.write(reinterpret_cast<char*>(&(data[i])), sizeof(Person));
    file_location += (sizeof(Person) * 2);
//    file_location = file.tellg();
  }


  file.close();
}

void readFile(fstream &file, int &file_location, vector<Person> &data){
  int i = 0;
  while (!file.eof() && i < 20){
    i++;
    Person p;
    //for some reason this function works on mac os but seg faults on arch, should be the same arch

    file.seekg(file_location, ios::beg);
    file.read(reinterpret_cast<char*>(&p), sizeof(Person));
    data.push_back(p);
    file_location += (sizeof(Person));


    /*
    file.seekg(file_location, ios::beg); // Move to the correct position
    char buffer[sizeof(Person)];
    file.read(buffer, sizeof(Person)); // Read binary data into buffer
    memcpy(&p, buffer, sizeof(Person)); // Copy binary data into p
    data.push_back(p);
    file_location = file.tellg(); // Update the file location
    */
  }
}
