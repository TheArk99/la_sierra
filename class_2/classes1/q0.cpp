#include <iostream>
using namespace std;
//////////////////////////////////////////////////////////////
// Class declaration
class Height {
private:
  int feet;
  int inches;
public:
//void InputHeight();
  Height(void);
  void OutputHeight();
  int operator+(Height &ht);
  bool EqualHeight(Height &ht);
};
//////////////////////////////////////////////////////////////
//class constructor
Height::Height(void){
  cout << "Enter feet and inches? ";
  cin >> feet >> inches;
}
// Class definition
/*
void Height::InputHeight() {
 cout << "Enter feet and inches? ";
 cin >> feet >> inches;
}
*/
//Height Height::AddHeight(Height ht){
//}
int Height::operator+(Height &ht){
  long heightAdded = feet + ht.feet;
  return heightAdded;
}
void Height::OutputHeight() {
 cout << feet << " feet " << inches << " inches" << endl;
}
bool Height::EqualHeight(Height &ht) {
if (feet == ht.feet && inches == ht.inches) {
 return true;
 }
else {
 return false;
 }
}
//////////////////////////////////////////////////////////////
// main
int main() {
  cout << "My height " << endl;
  Height myHeight;
  myHeight.OutputHeight();
  cout << "Your height " << endl;
  Height yourHeight;
  yourHeight.OutputHeight();
  if (myHeight.EqualHeight(yourHeight)) {
    cout << "We have the same height!" << endl;
  } else {
    cout << "We have different heights" << endl;
  }
  cout << myHeight + yourHeight << endl;
  return 0;
}
