/*
Create a Circle class with the following members:
 A data member for storing the radius of a circle.
 A member function for inputting a radius value.
 A member function for testing whether two circles have the same area or not. The
function returns a Boolean value.
 A member function that calculates and returns the area of the circle. The equation for the area is π r^2 where π is the constant 3.1415 and r is the radius.
*/

#include <iostream>
#include <cmath>
using namespace std;

//#define PI 3.1415
const double PI = 3.1415;


class Circle {
  private:
    double radius;
    double area;
  public:
    Circle(double getRadius);
    bool equalCircles(Circle circle);
    void areaCalc(void);
  //  void printRadius(void);
    friend ostream& operator<<(ostream& out, Circle circle);
};

//constructor
Circle::Circle(double getRadius){
  radius = getRadius;
  areaCalc();
}

//memeber fn def
bool Circle::equalCircles(Circle circle){
  bool isEqual = circle.area == area ? true : false;
  return isEqual;
}

void Circle::areaCalc(void){
  area = PI * (pow(radius, 2));
}

/*void Circle::printRadius(void){
  cout << radius << endl;
}*/
ostream& operator<<(ostream& out, Circle circle){
  out << circle.radius;
  return out;
}

int main(void){
  Circle circle1(5.222), circle2(5.222);
  string circlesEqual = circle1.equalCircles(circle2) ? "true" : "false";
  cout << circlesEqual << endl;
  cout << circle1 << endl;
  //circle1.printRadius();
  return 0;
}
