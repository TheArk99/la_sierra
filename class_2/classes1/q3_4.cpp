/*
Create a Temperature class with the following members:
 A data member for storing the degree of the temperature.
 A data member for storing the scale (either a C, F, or K for Celsius, Fahrenheit, or
Kelvin respectively).
 A member function for inputting a temperature value. Remember that a temperature
value consists of both a degree and a scale.
 A member function for outputting a temperature value.
 A member function for testing whether two temperature values are equal or not. The
function returns a Boolean value.
*/

#include <iostream>
using namespace std;

class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature(double getDegree, char getScale);
    void printTemp(void);
    bool equalTemps(Temperature temp);
};

//member fn
void Temperature::printTemp(void){
  cout << degree << scale << endl;
}

bool Temperature::equalTemps(Temperature temp){
  bool isEqual = degree == temp.degree && scale == temp.scale ? true : false;
  return isEqual;
}

//constructor
Temperature::Temperature(double getDegree, char getScale){
  degree = getDegree;
  scale = getScale;
}

int main(void){
  Temperature temp1(20, 'c'), temp2(20, 'c');
  string tempsAreEqual = temp1.equalTemps(temp2) ? "true" : "false";
  cout << tempsAreEqual << endl;
  return 0;
}
