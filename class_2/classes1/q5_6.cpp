/*
Declare a data structure for storing 10 temperature values.

Write the code in main to input 10 temperature values into the data structure declared in
question 5 and then print out these 10 temperature values.
*/

#include <iostream>
using namespace std;

class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
};

//member fn
/*void Temperature::printTemp(void){
  cout << degree << scale << endl;
}*/

bool Temperature::operator==(const Temperature& temp){
  if (scale != temp.scale)
    return false;

  return degree == temp.degree;
}

ostream& operator<<(ostream& out, const Temperature& temp){
  out << temp.degree << temp.scale;
  return out;
}

//constructor
//default
Temperature::Temperature() : degree(0.0), scale('c') {}
//explicitly called
Temperature::Temperature(double getDegree, char getScale){
  degree = getDegree;
  scale = getScale;
}

int main(void){
  Temperature temps[10] = {
    Temperature(23, 'c'),
    Temperature(27, 'c'),
    Temperature(28, 'c'),
    Temperature(29, 'c'),
    Temperature(21, 'c')
  };
  string tempsAreEqual = temps[0] == temps[1] ? "true" : "false";
  cout << tempsAreEqual << endl;
  cout << "Out of the 5 temps: " << endl;
  cout << temps[0] << endl;
  cout << temps[1] << endl;
  cout << temps[2] << endl;
  cout << temps[3] << endl;
  cout << temps[4] << endl;
  return 0;
}
