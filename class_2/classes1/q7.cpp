/*
Declare a data structure for storing 10 temperature values.

Write the code in main to input 10 temperature values into the data structure declared in
question 5 and then print out these 10 temperature values.

Continuing with question 6, input another temperature value and then search through the
10 temperature values to see if there is one that is equal. Print out an appropriate
message

*/

#include <iostream>
using namespace std;

class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
};

//member fn
/*void Temperature::printTemp(void){
  cout << degree << scale << endl;
}*/

bool Temperature::operator==(const Temperature& temp){
  if (scale != temp.scale)
    return false;

  return degree == temp.degree;
}

ostream& operator<<(ostream& out, const Temperature& temp){
  out << temp.degree << temp.scale;
  return out;
}

//constructor
//default
Temperature::Temperature() : degree(0.0), scale('c') {}
//explicitly called
Temperature::Temperature(double getDegree, char getScale){
  degree = getDegree;
  scale = getScale;
}

int main(void){
  Temperature temps[10] = {
    Temperature(23, 'c'),
    Temperature(27, 'c'),
    Temperature(28, 'c'),
    Temperature(29, 'c'),
    Temperature(21, 'c'),
    Temperature(24, 'c'),
    Temperature(24, 'c'),
    Temperature(39, 'c'),
    Temperature(32, 'c'),
    Temperature(30, 'c')
  };
  cout << "Out of the 10 temps: " << endl;
  for (int i = 1; i < 10; i++){
    if (temps[i-1] == temps[i]){
      cout << "temps[" << i-1 << "] == temps["<< i << "], value is: " << temps[i] << endl;
    }
  }
  return 0;
}
