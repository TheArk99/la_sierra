/*
Create a Circle class with the following members:
 A data member for storing the radius of a circle.
 A member function for inputting a radius value.
 A member function for testing whether two circles have the same area or not. The
function returns a Boolean value.
 A member function that calculates and returns the area of the circle. The equation for the area is π r^2 where π is the constant 3.1415 and r is the radius.
*/

#include <iostream>
#include <cmath>
#include "circleObj.h"
using namespace std;

int main(void){
  Circle circle1(5.222), circle2(5.222);
  //string circlesEqual = circle1.equalCircles(circle2) ? "true" : "false";
  string circlesEqual = circle1 == circle2 ? "true" : "false";
  cout << circlesEqual << endl;
  return 0;
}
