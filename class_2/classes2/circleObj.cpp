#include <cmath>
#include "circleObj.h"
using namespace std;

#define PI 3.1415

//constructor
Circle::Circle(double getRadius){
  radius = getRadius;
  areaCalc();
}

//memeber fn def
/*bool Circle::equalCircles(Circle circle){
  bool isEqual = circle.area == area ? true : false;
  return isEqual;
}*/

bool Circle::operator==(Circle &circle){
  bool isEqual = circle.area == area ? true : false;
  return isEqual;
}

void Circle::areaCalc(void){
  area = PI * (pow(radius, 2));
}
