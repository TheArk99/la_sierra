#ifndef __CIRCLE__
#define __CIRCLE__
class Circle {
  private:
    double radius;
    double area;
  public:
    Circle(double getRadius);
//    bool equalCircles(Circle circle);
    void areaCalc(void);
    bool operator==(Circle &circle);
};
#endif

