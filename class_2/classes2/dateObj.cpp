#include <ostream>
#include "dateObj.h"
using namespace std;

//constructor
Date::Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek){
  year = getYear;
  month = getMonth;
  day = getDay;
  dayOfWeek = getDayOfWeek;
}

//operator member func to print out Date obj
ostream& operator<<(ostream& out, Date& date){
  out << "Year: " << date.year << endl << "Month: " << date.month << endl << "Day: " << date.day << endl << "Day of week: " << date.dayOfWeek;
  return out;
}
