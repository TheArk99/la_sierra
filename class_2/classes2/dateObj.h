#ifndef __DATEOBJ__
#define __DATEOBJ__

#include <ostream>
using namespace std;

class Date {
  public:
    Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek);
    friend ostream& operator<<(ostream& out, Date& date);
  protected: //private except to inherited obj
    //Data members for storing the year, month, day and day of the week.
    unsigned int year;
    unsigned int month;
    unsigned int day;
    unsigned int dayOfWeek;
};
#endif

