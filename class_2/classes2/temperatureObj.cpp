#include <iostream>
#include "temperatureObj.h"
using namespace std;

//member fn
/*void Temperature::printTemp(void){
  cout << degree << scale << endl;
}*/

bool Temperature::operator==(const Temperature& temp){
  if (scale != temp.scale)
    return false;

  return degree == temp.degree;
}

ostream& operator<<(ostream& out, const Temperature& temp){
  out << temp.degree << temp.scale;
  return out;
}

//constructor
//default
Temperature::Temperature() : degree(0.0), scale('c') {}
//explicitly called
Temperature::Temperature(double getDegree, char getScale){
  degree = getDegree;
  scale = getScale;
}
