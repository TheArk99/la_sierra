#ifndef __TEMP__
#define __TEMP__

#include <ostream>
using namespace std;
class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
};

#endif

