/*
Overload the == operator for the Circle class so that you can do circle1 == circle2
Overload the >= operator for the Circle class
*/

#include <iostream>
#include <cmath>
#include "circleObj.h"
using namespace std;

int main(void){
  Circle circle1(5.222), circle2(5.222);
  string circlesEqual = circle1 == circle2 ? "true" : "false";
  cout << circlesEqual << endl;
  string circlesGreaterEqual = circle1 >= circle2 ? "true" : "false";
  cout << circlesGreaterEqual << endl;
  return 0;
}
