/*
Overload the == operator for the Circle class so that you can do circle1 == circle2
Overload the >= operator for the Circle class
*/


#ifndef __CIRCLE__
#define __CIRCLE__
class Circle {
  private:
    double radius;
    double area;
  public:
    Circle(double getRadius);
    void areaCalc(void);
    bool operator==(Circle &circle);
    bool operator>=(Circle &circle);
};
#endif

