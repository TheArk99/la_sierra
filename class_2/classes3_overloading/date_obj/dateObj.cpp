/*
Overload the == operator for the Date class
Overload the < operator for the Date class
*/


#include <ostream>
#include "dateObj.h"
using namespace std;

//constructor
Date::Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek){
  year = getYear;
  month = getMonth;
  day = getDay;
  dayOfWeek = getDayOfWeek;
}

//operator member func to print out Date obj
ostream& operator<<(ostream& out, Date& date){
  out << "Year: " << date.year << endl << "Month: " << date.month << endl << "Day: " << date.day << endl << "Day of week: " << date.dayOfWeek;
  return out;
}


bool operator==(Date& date1, Date& date2){
  bool datesEqual = date1.year == date2.year && date1.month == date2.month && date1.day == date2.day ? true : false;
  return datesEqual;
}

bool operator<(Date& date1, Date& date2){
  bool datesLess = date1.year < date2.year || date1.month < date2.month || date1.day < date2.day ? true : false;
  return datesLess;
}
