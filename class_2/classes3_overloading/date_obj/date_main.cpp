/*
Overload the == operator for the Date class
Overload the < operator for the Date class
*/

#include <iostream>
#include "dateObj.h"
using namespace std;

int main(void){
  Date dates[2] = {
    Date(2024, 1, 16, 4),
    Date(2024, 1, 17, 5)
  };
//  cout << date << endl;
  if (dates[0] == dates[1]){
    cout << "both dates are equivalant!!" << endl;
  }else if (dates[0] < dates[1]){
    cout << "date 1 is less than date 2" << endl;
  }else{
    cout << "most likley date 2 is less than date 1" << endl;
  }
  return 0;
}
