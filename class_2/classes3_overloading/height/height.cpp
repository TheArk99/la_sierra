/*
Overload the > operator for the Height class so that you can do myHeight > yourHeight
Overload the + operator for the Height class so that you can do myHeight + yourHeight
*/


#include <iostream>
#include "heightObj.h"
using namespace std;
int main() {
//  cout << "My height " << endl;
  Height myHeight;
  myHeight.inputHeight();
 // myHeight.OutputHeight();
  //cout << "Your height " << endl;
  Height yourHeight;
  yourHeight.inputHeight();
  /*
  yourHeight.OutputHeight();
  if (myHeight.EqualHeight(yourHeight)) {
    cout << "We have the same height!" << endl;
  } else {
    cout << "We have different heights" << endl;
  }
//  cout << myHeight + yourHeight << endl;
*/
  Height h;
  h = myHeight + yourHeight;
  cout << h << endl;
  if (myHeight > yourHeight){
    cout << "my height is greater than your height!" << endl;
  }else{
    cout << "my height is less than or equal to your height!" << endl;
  }
  return 0;
}
