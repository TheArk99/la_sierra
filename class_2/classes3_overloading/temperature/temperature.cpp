/*
Overload the == operator for the Temperature class
Overload the + operator for the Temperature class for adding two temperature values
*/

#include <iostream>
#include "temperatureObj.h"
using namespace std;

int main(void){
  Temperature temps[2] = {
    Temperature(-40, 'c'),
    Temperature(-40, 'f')
  };
  if (temps[0] == temps[1]){
    cout << "they are equal" << endl;
  }
  Temperature t1;
  t1 = temps[0] + temps[1];
  cout << t1 << endl;
  Temperature t2;
  t2 = temps[0] + 3;
  cout << t2 << endl;
  return 0;
}
