/*
Overload the == operator for the Temperature class
Overload the + operator for the Temperature class for adding two temperature values
*/


#ifndef __TEMP__
#define __TEMP__

#include <ostream>
using namespace std;
class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
    friend Temperature operator+(const Temperature& temp1, const Temperature& temp2);
    friend Temperature operator+(const Temperature& temp1, const double& temp2);
//    Temperature& operator=(const Temperature& t);
};

#endif

