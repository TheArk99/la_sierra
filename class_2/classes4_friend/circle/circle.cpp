/*
Overload the + operator for the Circle class where the implicit (first) operand is of a
different class such as int.

Overload the << and >> operators for the Circle class
*/

#include <iostream>
#include <cmath>
#include "circleObj.h"
using namespace std;

int main(void){
  //declaration and construct
  Circle circle1(5.222), circle2(5.222);
  //operator==
  string circlesEqual = circle1 == circle2 ? "true" : "false";
  cout << circlesEqual << endl;
  //operator>=
  string circlesGreaterEqual = circle1 >= circle2 ? "true" : "false";
  cout << circlesGreaterEqual << endl;
  //operator<<
  cout << circle1 << endl;

  Circle circle3, circle4;
  //operator>>
  cin >> circle3;
  //operator+
  circle4 = 3 + circle1;
  cout << circle4 << endl;
  return 0;
}
