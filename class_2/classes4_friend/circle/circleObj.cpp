/*
Overload the + operator for the Circle class where the implicit (first) operand is of a
different class such as int.

Overload the << and >> operators for the Circle class

*/


#include <cmath>
#include <iostream>
#include "circleObj.h"
using namespace std;

#define PI 3.1415

//constructor
Circle::Circle(void){
  radius = 0;
}
Circle::Circle(double getRadius){
  radius = getRadius;
  areaCalc();
}

//memeber fn def

bool Circle::operator==(const Circle &circle) const {
  bool isEqual = circle.area == area ? true : false;
  return isEqual;
}

void Circle::areaCalc(void){
  area = PI * (pow(radius, 2));
}

bool Circle::operator>=(const Circle &circle) const {
  bool isEqual = circle.area >= area ? true : false;
  return isEqual;
}

Circle operator+(const double& x, const Circle& circle){
  Circle c;
  c.radius = x + circle.radius;
  return c;
}


istream& operator>>(istream& in, Circle &circle){
  in >> circle.radius;
  return in;
}

ostream& operator<<(ostream& out, const Circle &circle){
  out << circle.radius;
  return out;
}
