/*
Overload the + operators for the Date class where the explicit (second) operand is an int.
How do you want the user to interpret this operation? In other words, what does it mean
to have a date object + an int?

Overload the << and >> operators for the Date class.
*/



#include <iostream>
#include "dateObj.h"
using namespace std;

//constructor
Date::Date(void){
  year = 0;
  month = 0;
  day = 0;
  dayOfWeek = 0;
}

Date::Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek){
  year = getYear;
  month = getMonth;
  day = getDay;
  dayOfWeek = getDayOfWeek;
}

//operator member func to print out Date obj
ostream& operator<<(ostream& out, const Date& date){
  out << "Year: " << date.year << endl << "Month: " << date.month << endl << "Day: " << date.day << endl << "Day of week: " << date.dayOfWeek;
  return out;
}


bool operator==(const Date& date1, const Date& date2){
  bool datesEqual = date1.year == date2.year && date1.month == date2.month && date1.day == date2.day ? true : false;
  return datesEqual;
}

bool operator<(const Date& date1, const Date& date2){
  bool datesLess = date1.year < date2.year || date1.month < date2.month || date1.day < date2.day ? true : false;
  return datesLess;
}


istream& operator>>(istream& in, Date& date){
  in >> date.year >> date.month >> date.day >> date.dayOfWeek;
  return in;
}


Date operator+(const Date& date1, const unsigned int& day){
  Date date;
  date.year = date1.year;
  date.month = date1.month;
  date.day = date1.day + day;
  date.dayOfWeek = date1.dayOfWeek + day > 7 ? date.dayOfWeek - 7 : date.dayOfWeek + day;
  return date;
}
