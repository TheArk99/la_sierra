/*
Overload the + operators for the Date class where the explicit (second) operand is an int.
How do you want the user to interpret this operation? In other words, what does it mean
to have a date object + an int?

Overload the << and >> operators for the Date class.
*/



#ifndef __DATEOBJ__
#define __DATEOBJ__

#include <iostream>
using namespace std;

class Date {
  protected: //private except to inherited obj
    //Data members for storing the year, month, day and day of the week.
    unsigned int year;
    unsigned int month;
    unsigned int day;
    unsigned int dayOfWeek;
  public:
    Date(void);
    Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek);
    friend bool operator==(const Date& date1, const Date& date2);
    friend bool operator<(const Date& date1, const Date& date2);
    friend Date operator+(const Date& date1, const unsigned int& day);
    friend ostream& operator<<(ostream& out, const Date& date);
    friend istream& operator>>(istream& in, Date& date);
};
#endif

