/*
Overload the + operators for the Date class where the explicit (second) operand is an int.
How do you want the user to interpret this operation? In other words, what does it mean
to have a date object + an int?

Overload the << and >> operators for the Date class.
*/

#include <iostream>
#include "dateObj.h"
using namespace std;

int main(void){
  Date dates[4] = {
    Date(2024, 1, 16, 4),
    Date(2024, 1, 17, 5),
  };
//  cout << date << endl;
  if (dates[0] == dates[1]){
    cout << "both dates are equivalant!!" << endl;
  }else if (dates[0] < dates[1]){
    cout << "date 1 is less than date 2" << endl;
  }else{
    cout << "most likley date 2 is less than date 1" << endl;
  }
  cin >> dates[2];
  dates[3] = dates[2] + 3;
  cout << dates[3] << endl;
  return 0;
}
