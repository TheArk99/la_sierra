/*
Overload the - operator for the Height class where the implicit (first) operand is of a
different class such as int.
*/


#include <iostream>
#include "heightObj.h"
using namespace std;
//class constructor
Height::Height(void) : feet(0), inches(0) {}

// Class definition
void Height::inputHeight(void){
  cout << "Enter feet and inches? ";
  cin >> feet >> inches;
}

void Height::OutputHeight(void) {
  cout << feet << " feet " << inches << " inches" << endl;
}

bool Height::EqualHeight(const Height &ht) {
  if (feet == ht.feet && inches == ht.inches) {
    return true;
   }else {
    return false;
   }
}

Height operator+(const Height &ht1, const Height &ht2){
  Height h;
  h.feet = ht1.feet + ht2.feet;
  h.inches = ht1.inches + ht2.inches;
  while(h.inches >= 12){
    h.inches -= 12;
    h.feet++;
  }
  return h;
}

Height operator-(const int& ht1, const Height &ht2){
  Height h;
  h.feet = ht2.feet;
  h.inches = ht1 - ht2.inches;
  if (h.inches < 0){
    h.inches += 12;
    h.feet--;
  }
  return h;
}

bool operator>(const Height &ht1, const Height &ht2){
  bool heightGreater = ht1.feet >= ht2.feet && ht1.inches > ht2.inches ? true : false;
  return heightGreater;

}

ostream& operator<<(ostream& out, const Height &ht){
  out << ht.feet << "\"" << ht.inches;
  return out;
}
