/*
Overload the + operators for the Temperature class where the implicit (first) operand is of
a different class such as int

Overload the << and >> operators for the Temperature class
*/

#include <iostream>
#include "temperatureObj.h"
using namespace std;

int main(void){
  Temperature temps[3] = {
    Temperature(-40, 'c'),
    Temperature(-40, 'f')
  };
  if (temps[0] == temps[1]){
    cout << "they are equal" << endl;
  }
  cin >> temps[2];
  Temperature t1;
  t1 = 33 + temps[2];
  cout << t1 << endl;
//  Temperature t2;
 // t2 = temps[0] + 3;
  //cout << t2 << endl;

  return 0;
}
