/*
Overload the + operators for the Temperature class where the implicit (first) operand is of
a different class such as int

Overload the << and >> operators for the Temperature class
*/



#ifndef __TEMP__
#define __TEMP__

#include <iostream>
using namespace std;
class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
    friend istream& operator>>(istream& in, Temperature& temp);
    friend Temperature operator+(const Temperature& temp1, const Temperature& temp2);
    friend Temperature operator+(const Temperature& temp1, const double& temp2);
    friend Temperature operator+(const double& temp1, const Temperature& temp2);
//    Temperature& operator=(const Temperature& t);
};

#endif

