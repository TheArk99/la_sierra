/*
Overload the ++ operator for the Circle class

Overload the -- operator for the Circle class
*/



#ifndef __CIRCLE__
#define __CIRCLE__
#include <iostream>
using namespace std;
class Circle {
  private:
    double radius;
    double area;
  public:
    Circle(void);
    Circle(double getRadius);
    void areaCalc(void);
    bool operator==(const Circle &circle) const;
    bool operator>=(const Circle &circle) const;
    Circle operator++();
    Circle operator++(int);
    Circle operator--();
    Circle operator--(int);
    friend ostream& operator<<(ostream& out, const Circle &circle);
    friend istream& operator>>(istream& in, Circle &circle);
    friend Circle operator+(const double& x, const Circle& circle);
};
#endif

