/*
Overload the ++ operator for the Date class. This will add one day to the current date.
Check for rollovers at the end of the month
*/




#ifndef __DATEOBJ__
#define __DATEOBJ__

#include <iostream>
using namespace std;

class Date {
  protected: //private except to inherited obj
    //Data members for storing the year, month, day and day of the week.
    unsigned int year;
    unsigned int month;
    unsigned int day;
    unsigned int dayOfWeek;
  public:
    Date(void);
    Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek);
    Date operator++(void);
    Date operator++(int);
    friend bool operator==(const Date& date1, const Date& date2);
    friend bool operator<(const Date& date1, const Date& date2);
    friend Date operator+(const Date& date1, const unsigned int& day);
    friend ostream& operator<<(ostream& out, const Date& date);
    friend istream& operator>>(istream& in, Date& date);
};
#endif

