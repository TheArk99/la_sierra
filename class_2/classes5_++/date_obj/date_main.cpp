/*
Overload the ++ operator for the Date class. This will add one day to the current date.
Check for rollovers at the end of the month
*/

#include <iostream>
#include "dateObj.h"
using namespace std;

int main(void){
  Date dates[4] = {
    Date(2024, 1, 16, 4),
    Date(2024, 1, 17, 5),
  };
//  cout << date << endl;
  if (dates[0] == dates[1]){
    cout << "both dates are equivalant!!" << endl;
  }else if (dates[0] < dates[1]){
    cout << "date 1 is less than date 2" << endl;
  }else{
    cout << "most likley date 2 is less than date 1" << endl;
  }
  cin >> dates[2];
  dates[3] = dates[2] + 1;
  cout << dates[3] << endl << endl;
  cout << ++dates[3] << endl;
  return 0;
}
