/*
Overload the -- operator for the Height class. This will subtract one inch from the height
*/


#include <iostream>
#include "heightObj.h"
using namespace std;
int main() {
//  cout << "My height " << endl;
  Height myHeight;
  myHeight.inputHeight();
 // myHeight.OutputHeight();
  //cout << "Your height " << endl;
  Height yourHeight;
  yourHeight.inputHeight();
  /*
  yourHeight.OutputHeight();
  if (myHeight.EqualHeight(yourHeight)) {
    cout << "We have the same height!" << endl;
  } else {
    cout << "We have different heights" << endl;
  }
//  cout << myHeight + yourHeight << endl;
*/
  Height h;
  h = myHeight + yourHeight;
  cout << h << endl;
  if (myHeight > yourHeight){
    cout << "my height is greater than your height!" << endl;
  }else{
    cout << "my height is less than or equal to your height!" << endl;
  }
  cout << endl << "My Height currently is: " << myHeight<< endl;
  h = --myHeight;
  cout << "My height is now: " << h << endl;
  return 0;
}
