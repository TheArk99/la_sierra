/*
Overload the -- operator for the Height class. This will subtract one inch from the height
*/

#ifndef __HEIGHT__
#define __HEIGHT__

#include <ostream>
using namespace std;
// Class declaration
class Height {
private:
  int feet;
  int inches;
public:
  Height(void);
  Height(const int& i, const int& f);
  void inputHeight(void);
  void OutputHeight(void);
  bool EqualHeight(const Height &ht);
  Height operator--(void);
  Height operator--(int);
  friend Height operator+(const Height &ht1, const Height &ht2);
  friend Height operator-(const int& ht1, const Height &ht2);
  friend bool operator>(const Height &ht1, const Height &ht2);
  friend ostream& operator<<(ostream& out, const Height &ht);
};

#endif

