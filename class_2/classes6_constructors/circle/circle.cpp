/*
Add the constructor for the Circle class. Add in extra code for error checking to make
sure that the initialization values are valid.
*/

#include <iostream>
#include "circleObj.h"
using namespace std;

int main(void){
  //declaration and construct
  Circle circle1(5.222), circle2(5.222);
  //operator==
  string circlesEqual = circle1 == circle2 ? "true" : "false";
  cout << circlesEqual << endl;
  //operator>=
  string circlesGreaterEqual = circle1 >= circle2 ? "true" : "false";
  cout << circlesGreaterEqual << endl;
  //operator<<
  cout << circle1 << endl;

  Circle circle3, circle4;
  //operator>>
  cin >> circle3;
  //operator+
  circle4 = 3 + circle3;
  cout << circle4 << endl;
  cout << circle4++ << endl;
  cout << ++circle4 << endl;
  cout << circle4 << endl;
  cout << --circle4 << endl;
  cout << circle4-- << endl;
  cout << circle4 << endl;
  return 0;
}
