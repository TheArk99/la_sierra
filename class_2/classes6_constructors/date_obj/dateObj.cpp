/*
Add the constructor for the Date class. Add in extra code for error checking to make sure
that the initialization values are valid.
*/



#include <iostream>
#include <ctime>
#include "dateObj.h"
using namespace std;

//constructor
Date::Date(void){
  year = 0;
  month = 0;
  day = 0;
  dayOfWeek = 0;
}

Date::Date(unsigned int getYear, unsigned int getMonth, unsigned int getDay, unsigned int getDayOfWeek){
  year = getYear;
  month = getMonth;
  day = getDay;
  dayOfWeek = getDayOfWeek;
}

//operator member func to print out Date obj
ostream& operator<<(ostream& out, const Date& date){
  out << "Year: " << date.year << endl << "Month: " << date.month << endl << "Day: " << date.day << endl << "Day of week: " << date.dayOfWeek;
  return out;
}


bool operator==(const Date& date1, const Date& date2){
  bool datesEqual = date1.year == date2.year && date1.month == date2.month && date1.day == date2.day ? true : false;
  return datesEqual;
}

bool operator<(const Date& date1, const Date& date2){
  bool datesLess = date1.year < date2.year || date1.month < date2.month || date1.day < date2.day ? true : false;
  return datesLess;
}


istream& operator>>(istream& in, Date& date){
  in >> date.year >> date.month >> date.day >> date.dayOfWeek;
  return in;
}


Date operator+(const Date& date1, const unsigned int& day){
  Date date;
  date.year = date1.year;
  date.month = date1.month;
  date.day = date1.day + day;
  date.dayOfWeek = date1.dayOfWeek + day;
  date.dayOfWeek = date1.dayOfWeek > 7 ? date.dayOfWeek - 7 : date.dayOfWeek;
  tm lastDayOfMonth = {0};
  lastDayOfMonth.tm_year = date.year - 1900;
  lastDayOfMonth.tm_mon = date.month;
  lastDayOfMonth.tm_mday = 0;
  mktime(&lastDayOfMonth);
  lastDayOfMonth.tm_mday--;
  int lastDay = lastDayOfMonth.tm_mday;
  if (date.day > lastDay + 1){
    date.day = 1;
    date.month++;
  }
  return date;
}


Date Date::operator++(void){
  day++;
  dayOfWeek++;
  dayOfWeek = dayOfWeek > 7 ? dayOfWeek - 7 : dayOfWeek;
  tm lastDayOfMonth = {0};
  lastDayOfMonth.tm_year = year - 1900;
  lastDayOfMonth.tm_mon = month - 1;
  lastDayOfMonth.tm_mday = 0;
  mktime(&lastDayOfMonth);
  lastDayOfMonth.tm_mday--;
  int lastDay = lastDayOfMonth.tm_mday;
  if (day > lastDay + 1){
    day = 1;
    month++;
  }
  return *this;
}

Date Date::operator++(int){
  Date d(year, month, day, dayOfWeek);
  day++;
  dayOfWeek++;
  dayOfWeek = dayOfWeek > 7 ? dayOfWeek - 7 : dayOfWeek;
  tm lastDayOfMonth = {0};
  lastDayOfMonth.tm_year = year - 1900;
  lastDayOfMonth.tm_mon = month - 1;
  lastDayOfMonth.tm_mday = 0;
  mktime(&lastDayOfMonth);
  lastDayOfMonth.tm_mday--;
  int lastDay = lastDayOfMonth.tm_mday;
  if (day > lastDay + 1){
    day = 1;
    month++;
  }
  return d;
}
