/*
Add the constructor for the Temperature class. Add in extra code for error checking to
make sure that the initialization values are valid.
*/

#include <iostream>
#include "temperatureObj.h"
#include "convertScale.h"
using namespace std;

//member fn
/*void Temperature::printTemp(void){
  cout << degree << scale << endl;
}*/

bool Temperature::operator==(const Temperature& temp){
  if (scale != temp.scale){
    double degree1 = degree;
    double degree2 = temp.degree;
    convertTwoDegrees(scale, temp.scale, degree1, degree2); //c++ syntax correct way, by refernce
//    convertTwoDegrees(&degree1, &degree2); //c way, by pointers
    return degree1 == degree2;

  }else{
    return degree == temp.degree;
  }
}

ostream& operator<<(ostream& out, const Temperature& temp){
  out << temp.degree << temp.scale;
  return out;
}

istream& operator>>(istream& in, Temperature& temp){
  in >> temp.degree >> temp.scale;
  return in;
}

Temperature operator+(const Temperature& temp1, const Temperature& temp2){
  double degree1 = temp1.degree;
  double degree2 = temp2.degree;
  if (temp1.scale != 'c' || temp2.scale != 'c'){
    convertTwoDegrees(temp1.scale, temp2.scale, degree1, degree2); //c++ syntax correct way, by refernce
//    convertTwoDegrees(&degree1, &degree2); //c way, by pointers
  }

  //Temperature temp(degree1 + degree2, 'c');
  Temperature temp;
  temp.degree = degree1 + degree2;

  return temp;
}

Temperature operator+(const Temperature& temp1, const double& temp2){
  double degree1 = temp1.degree;
  double degree2 = temp2;

  //Temperature temp(degree1 + degree2, 'c');
  Temperature temp;
  temp.degree = degree1 + degree2;

  return temp;
}

Temperature operator+(const double& temp1, const Temperature& temp2){
  double degree1 = temp2.degree;
  double degree2 = temp1;

  //Temperature temp(degree1 + degree2, 'c');
  Temperature temp;
  temp.degree = degree1 + degree2;

  return temp;
}

/*
Temperature& Temperature::operator=(const Temperature& t){
  degree = t.degree;
  return *this;
}*/

//constructor
//default
Temperature::Temperature() : degree(0.0), scale('c') {}
//explicitly called
Temperature::Temperature(double getDegree, char getScale){
  degree = getDegree;
  scale = getScale;
}


Temperature Temperature::operator--(void){
  --degree;
  return *this;
}

Temperature Temperature::operator--(int){
  Temperature t(degree, scale);
  degree--;
  return t;
}

Temperature Temperature::operator++(int){
  Temperature t(degree, scale);
  degree++;
  return t;
}


Temperature Temperature::operator++(void){
  ++degree;
  return *this;
}

Temperature Temperature::operator-(void){
  degree = degree * -1;
  return *this;
}

Temperature Temperature::operator-(int){
  Temperature t(degree, scale);
  degree = degree * -1;
  return t;
}
