#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

/*
Write a program to open a sequential file that will append a line of text to the end of the
file each time that the program is run. The program first asks the user to enter a line of
text. This line is then added (appended) to the end of the existing file, so the file gets
longer and longer. Run the program five or more times and see that the file has five or
more lines.
*/

int main(int argv, char** argc){
  fstream file;
  string file_name;
  string file_contents_to_write = "";
  string file_contents;

  if (argv >= 2){
    file_name = argc[1];
    if (argv > 2){
      file_contents_to_write = argc[2];
    }
  }else{
    cout << "Enter the name of the file to write to: ";
    getline(cin, file_name);

  }

  if (file_contents_to_write == ""){
    cout <<  "Enter what content you would like to put in file: ";
    getline(cin, file_contents_to_write);
  }

  file.open(file_name, ios::app);

  if (!file.is_open()) {
    cout << "Error opening file." << endl;
    return 1;
  }

//  file.write(reinterpret_cast<char*>(&file_contents_to_write), sizeof(file_contents_to_write));
  file << endl << file_contents_to_write;

  file.close();
  file.open(file_name, ios::in);


  cout << endl << "The file contents of " << file_name << ": " << endl;
  while(getline(file, file_contents)){
    cout << file_contents << endl;
  }
  file.close();

}
