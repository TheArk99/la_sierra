#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

/*
Write a program to make a copy of the file created in question 1. You will open the file
created in question 1 for reading and open another file for writing. Read the lines from
the first file and write it into the second file.
*/

int main(int argv, char** argc){
  fstream file_to_copy;
  fstream file_to_write;
  string file_to_copy_name;
  string file_to_write_name = "";
  string file_contents;

  if (argv >= 2){
    file_to_copy_name = argc[1];
    if (argv > 2){
      file_to_write_name = argc[2];
    }
  }else{
    cout << "Enter the name of the file to copy from: ";
    getline(cin, file_to_copy_name);
  }

  if (file_to_write_name == ""){
    cout << "Enter the name of the new file: ";
    getline(cin, file_to_write_name);
  }

  file_to_copy.open(file_to_copy_name, ios::in);
  file_to_write.open(file_to_write_name, ios::out);

  if (!file_to_copy.is_open()) {
    cout << "Error opening file." << endl;
    return 1;
  }

  while(getline(file_to_copy, file_contents)){
    file_to_write << file_contents << endl;
  }


  file_to_copy.close();
  file_to_write.close();


}
