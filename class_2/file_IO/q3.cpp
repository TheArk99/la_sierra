#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

/*
Implement the example random access file program in section 6.
*/

struct Person {
  int data;
  string name;
};

int main(int argv, char** argc){
  fstream file;
  string file_name;
//  string file_contents_to_write = "";
 // string file_contents;

  Person p1;
  p1.data = 22;
  p1.name = "something";

  if (argv >= 2){
    file_name = argc[1];
    /*
    if (argv > 2){
      file_contents_to_write = argc[2];
    }
    */
  }else{
    cout << "Enter the name of the file to write to: ";
    getline(cin, file_name);

  }

  /*
  if (file_contents_to_write == ""){
    cout <<  "Enter what content you would like to put in file: ";
    getline(cin, file_contents_to_write);
  }
  */

  file.open(file_name, ios::in | ios::out | ios::binary);

  if (!file.is_open()) {
    cout << "Error opening file." << endl;
    return 1;
  }

  file.seekp(0, ios::beg);
  file.write(reinterpret_cast<char*>(&p1), sizeof(p1));

  Person p2;
  file.seekg(0, ios::beg);
  file.read(reinterpret_cast<char*>(&p2), sizeof(p2));

  cout << p2.name << endl;


  file.close();

}
