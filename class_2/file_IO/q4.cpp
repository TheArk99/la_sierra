#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

/*
Continuing from the random access file program, add a menu for the user to select from
one of five options: 1) to add a new record to a given record number; 2) to print out the
information for a record for a given record number; 3) to change the information for a
given record number; 4) to delete a record for a given record number; and 5) to exit the
program. For options 1, 2, 3 and 4, the program needs to ask the user to enter a record
number. For options 1 and 3, the program will further ask the user for the new
information for the record
*/

struct Person {
  int data;
  string name;
};

int main(int argv, char** argc){
  fstream file;
  string file_name = "testFile.txt";
  int postion_in_file = 0;

  Person p1;
  p1.data = 22;
  p1.name = "something";

  if (argv >= 2){
    file_name = argc[1];
  }

  /*
  if (argv > 2){
    user_input = argc[2];
  }else{
  }
*/

  file.open(file_name, ios::in | ios::out | ios::binary);

  if (!file.is_open()) {
    cout << "Error opening file." << endl;
    return 1;
  }

  file.seekp(postion_in_file, ios::beg);
  file.write(reinterpret_cast<char*>(&p1), sizeof(p1));
  postion_in_file += sizeof(Person) + 1;

  Person temp;
  bool status = true;
  while (status == true) {
    int choice = 0;
    int record;
    string user_input;
    string s;
    cout << "1) to add a new record to a given record number\n2) to print out the information for a record for a given record number\n3) to change the information for a given record number\n4) to delete a record for a given record number\n5) to exit the program." << endl;
    getline(cin, user_input);
    choice = stoi(user_input);
    switch(choice){
      case 1:
        cout << "Enter members: " << endl;
        cout << "Enter data: ";
        getline(cin, s);
        temp.data = stoi(s);
        cout << "Enter name: ";
        getline(cin, temp.name);
        file.seekp(postion_in_file, ios::beg);
        file.write(reinterpret_cast<char*>(&temp), sizeof(Person));
        postion_in_file += sizeof(Person) + 1;
        break;
      case 2:
        file.seekg(postion_in_file, ios::beg);
        file.read(reinterpret_cast<char*>(&temp), sizeof(Person));
        cout << temp.data << " " << temp.name << endl;
        break;
      case 3:
        cout << "Enter record to change: ";
        getline(cin, user_input);
        record = stoi(user_input);
        cout << "Enter new data:";
        getline(cin, s);
        temp.data = stoi(s);
        cout << "Enter ";
        break;
      case 5:
        status = false;
        break;
    }
    cout << "Hit Enter to continue!";
    getline(cin, user_input);
    system("clear");
  }

  file.close();

}
