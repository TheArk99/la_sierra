#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
using namespace std;

int main(int argv, char** argc){
  fstream file;
  string file_name;
  string file_contents;
  if (argv >= 2){
    file_name = argc[1];
  }else{
    cout << "Enter the name of the file to read from: ";
    cin >> file_name;
  }
  file.open(file_name, ios::in);
  if (!file.is_open()) {
    cout << "Error opening file." << endl;
    return 1;
  }
  cout << "The file contents of file " << file_name << ": " << endl;
  while(getline(file, file_contents)){
    cout << file_contents << endl;
  }
  file.close();
}
