#include "swapFunc.h"
#include "height/heightObj.h"
// I guess this file is not needed when working with templates, as I run into linking problems if i do not explicitly declare it which then sort of defeats the purpose of the template
template <class T>
void swapFunc(T& x, T& y){
  T temp;
  temp = y;
  y = x;
  x = temp;
}

template void swapFunc<Height>(Height&, Height&);
