#ifndef __SWAPFUNCH__
#define __SWAPFUNCH__

template <class T>
//void swapFunc(T& x, T& y);
void swapFunc(T& x, T& y){
  T temp;
  temp = y;
  y = x;
  x = temp;
}



#endif

