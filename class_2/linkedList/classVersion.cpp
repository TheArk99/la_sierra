#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include "temperature/temperatureObj.h"
using namespace std;

// Define a Node structure
struct Node {
  int data;
  Node* next;
  Node* prev;

  // Constructor
  Node(int d) : data(d), next(nullptr), prev(nullptr) {}
};

// Define LinkedList class
class LinkedList {
private:
  Node* Head;
  Node* Tail;

public:
  // Constructor
  LinkedList() : Head(nullptr), Tail(nullptr) {}

  // Function to insert at the front
  void insertFront(int data) {
      Node* newNode = new Node(data);
      if (Head == nullptr) {
          Head = newNode;
          Tail = newNode;
      } else {
          newNode->next = Head;
          Head->prev = newNode;
          Head = newNode;
      }
  }

  // Function to insert at the end
  void insertEnd(int data) {
      Node* newNode = new Node(data);
      if (Tail == nullptr) {
          Head = newNode;
          Tail = newNode;
      } else {
          Tail->next = newNode;
          newNode->prev = Tail;
          Tail = newNode;
      }
  }

  // Function to print the list
  void display_start() {
      Node* current = Head;
      while (current != nullptr) {
          std::cout << current->data << " ";
          current = current->next;
      }
      std::cout << std::endl;
  }

  void display_end() {
      Node* current = Tail;
      while (current != nullptr) {
          std::cout << current->data << " ";
          current = current->prev;
      }
      std::cout << std::endl;
  }

  //function to search the list
  Node *searchList(const int& num){
    Node* temp = Head;
    while (temp != nullptr){
      if (temp->data == num){
        return temp;
      }
      temp = temp->next;
    }
    return nullptr;
  }

  //function to del a node
  void rmNode(Node **toDel){
    if (Head == *toDel){
      Head = (*toDel)->next;
      if (Head != nullptr)
        Head->prev = nullptr;
      delete *toDel;
      return;
    }else{
      (*toDel)->prev->next = (*toDel)->next;
      if ((*toDel)->next != nullptr)
        (*toDel)->next->prev = (*toDel)->prev;
      delete *toDel;
    }
  }

  void toDoWithNode(int& num){
    Node *findNode = searchList(num);
    if (findNode != nullptr){
      cout << "found: " << findNode->data << endl;
      cout << "would you like to replace num? [y/n] ";
      char answer;
      cin >> answer;

      if (answer == 'Y' || answer == 'y'){
        cout << "enter new num: ";
        int newNum;
        cin >> newNum;
        findNode->data = newNum;
      }else{

        cout << "would you like to delete num? [y/n] ";
        cin >> answer;

        if (answer == 'Y' || answer == 'y'){
          cout << "deleting..." << endl;
          rmNode(&findNode);
        }

      }

    }

  }

};

int main() {
  LinkedList list;

  // Insert elements at the front
  list.insertFront(3);
  list.insertFront(2);
  list.insertFront(1);

  // Insert elements at the end
  list.insertEnd(4);
  list.insertEnd(5);

  int numToSearch;
  cout << "Enter num to find in list: ";
  cin >> numToSearch;
  list.toDoWithNode(numToSearch);

  cout << "List from start to end: ";
  list.display_start();

  cout << "List from end to start: ";
  list.display_end();

  return 0;
}
