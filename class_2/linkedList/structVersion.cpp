#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include "temperature/temperatureObj.h"
using namespace std;

struct ListNode{
//  int data;
  Temperature data;
  ListNode* next;
  ListNode* previous;
};

//ListNode *genNode(int data){
ListNode *genNode(Temperature data){
  ListNode* temp = new(ListNode);
  temp->data = data;
  temp->next = nullptr;
  temp->previous = nullptr;
  return temp;
}


void printList(ListNode& Head){
  ListNode* list = &Head;
  while (list != nullptr){
    cout << list->data << " ";
    list = list->next;
  }
  cout << endl;
}


//ListNode *searchList(const int& num, ListNode& Head){

ListNode *searchList(Temperature num, ListNode& Head){
  ListNode* temp = &Head;
  while (temp != nullptr){
    if (temp->data == num){
      return temp;
    }
    temp = temp->next;
  }
  return nullptr;
}


void rmNode(ListNode **toDel, ListNode **Head){
  if (*Head == *toDel){
    *Head = (*toDel)->next;
    if (*Head != nullptr)
      (*Head)->previous = nullptr;
    delete *toDel;
    return;
  }else{
    (*toDel)->previous->next = (*toDel)->next;
    if ((*toDel)->next != nullptr)
      (*toDel)->next->previous = (*toDel)->previous;
    delete *toDel;
  }
}

int main(int argc, char** argv){
//  srand(time(0));
  ListNode* Head = nullptr;
  ListNode* node = nullptr;

//  node = genNode(rand() % 100 + 1);
  node = genNode(Temperature(5,'c'));
  node->previous = nullptr;
  Head = node;
  node = genNode(Temperature(4,'c'));
  node->next = Head;
  Head->previous = node;
  Head = node;
  node = genNode(Temperature(3,'c'));
  node->next = Head;
  Head->previous = node;
  Head = node;
  node = genNode(Temperature(2,'c'));
  node->next = Head;
  Head->previous = node;
  Head = node;
  node = genNode(Temperature(1,'c'));
  node->next = Head;
  Head->previous = node;
  Head = node;



  ListNode* findNode;
  int numToSearch;
  cout << "Enter num to find in list: ";
  cin >> numToSearch;
  findNode = searchList(Temperature(numToSearch,'c'), *Head);
  if (findNode != nullptr){
    cout << "found: " << findNode->data << endl;
    cout << "would you like to replace num? [y/n] ";
    char answer;
    cin >> answer;

    if (answer == 'Y' || answer == 'y'){
      cout << "enter new num: ";
      int newNum;
      cin >> newNum;
      Temperature newTemp(newNum, 'c');
      findNode->data = newTemp;
    }else{

      cout << "would you like to delete num? [y/n] ";
      cin >> answer;

      if (answer == 'Y' || answer == 'y'){
        cout << "deleting..." << endl;
        rmNode(&findNode, &Head);
      }

    }

  }


  printList(*Head);

  return 0;
}
