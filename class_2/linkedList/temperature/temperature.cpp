/*
Add the constructor for the Temperature class. Add in extra code for error checking to
make sure that the initialization values are valid.
*/

#include <iostream>
#include "temperatureObj.h"
using namespace std;

int main(void){
  Temperature temps[3] = {
    Temperature(-40, 'c'),
    Temperature(-40, 'f')
  };
  if (temps[0] == temps[1]){
    cout << "they are equal" << endl;
  }
  cin >> temps[2];
  Temperature t1;
  t1 = temps[2]; //+ 3;
  cout << -t1 << endl;
//  Temperature t2;
 // t2 = temps[0] + 3;
  //cout << t2 << endl;

  return 0;
}
