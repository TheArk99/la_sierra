/*
Add the constructor for the Temperature class. Add in extra code for error checking to
make sure that the initialization values are valid.
*/


#ifndef __TEMP__
#define __TEMP__

#include <iostream>
using namespace std;
class Temperature{
  private:
    double degree;
    char scale;
  public:
    Temperature();
    Temperature(double getDegree, char getScale);
  //  void printTemp(void);
    bool operator==(const Temperature& temp);
    Temperature operator--(void);
    Temperature operator--(int);
    Temperature operator++(int);
    Temperature operator++(void);
    Temperature operator-(int);
    Temperature operator-(void);
    friend ostream& operator<<(ostream& out, const Temperature& temp);
    friend istream& operator>>(istream& in, Temperature& temp);
    friend Temperature operator+(const Temperature& temp1, const Temperature& temp2);
    friend Temperature operator+(const Temperature& temp1, const double& temp2);
    friend Temperature operator+(const double& temp1, const Temperature& temp2);
//    Temperature& operator=(const Temperature& t);
};

#endif

