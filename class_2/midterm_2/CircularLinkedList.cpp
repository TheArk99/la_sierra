#include "CircularLinkedList.h"

CircularLinkedList::CircularLinkedList()
{
    Head = nullptr;
    Tail = nullptr;
}

CircularLinkedList::~CircularLinkedList() {
    ListNode* current = Head;
    if (current != nullptr) {
        ListNode* next;
        do {
            next = current->next;
            delete(current);
            current = next;
        } while (current != Head && current != nullptr);
    }
    Head = nullptr;

}


void CircularLinkedList::InsertHead(Temperature t)
{

  /*
    // find the tail in order to change the tail link to the new head
    ListNode* newNode = nullptr;
    if (Head != nullptr) {  // non empty list
        ListNode* pNewNode = nullptr;
        newNode = Head;
        bool start = true;
        while (newNode != Head || start) {
            cout << "traversing " << newNode->data << endl;
            start = false;
            pNewNode = newNode;
            newNode = newNode->next;
        }
        // pNewNode now should be pointing to the tail
        newNode = new(ListNode);
        newNode->data = t;
        newNode->next = Head;
        Head = newNode;
        pNewNode->next = Head;
    } else {    // empty list
        newNode = new(ListNode);
        Head = newNode;
        newNode->data = t;
        newNode->next = Head;
    }
    */

    ///*
  ListNode* newNode = new ListNode;
  newNode->data = t;
  if (Head == nullptr) {
      Head = newNode;
      Tail = newNode;
      newNode->next = Head;
  } else {
      newNode->next = Head;
      Head = newNode;
      Tail->next = Head;
  }
  //*/

}

ostream& operator<<(ostream &left, const CircularLinkedList &right){
    ListNode* ptr = right.Head;
    if (right.Head != nullptr) {   // non empty list
        bool start = true;
        while (ptr != right.Head || start) {
          if (ptr != nullptr) {   // non empty node
            start = false;
            left << "cout: " << ptr->data << ", ";
            ptr = ptr->next;
          }else {
            break;
          }
        }
    }

    return left;

}
