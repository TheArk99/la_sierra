#ifndef CIRCULARLINKEDLIST_H
#define CIRCULARLINKEDLIST_H
#include <iostream>
#include "temperature/temperature.h"
using namespace std;

struct ListNode {
    Temperature data;
    ListNode *next = nullptr;
    ListNode *prev = nullptr;
};

class CircularLinkedList
{
    public:
        CircularLinkedList();
        void InsertHead(Temperature t);
        friend ostream& operator<<(ostream &left, const CircularLinkedList &right);
        virtual ~CircularLinkedList();

    protected:

    private:
        ListNode *Head;
        ListNode *Tail;

};

#endif // CIRCULARLINKEDLIST_H

