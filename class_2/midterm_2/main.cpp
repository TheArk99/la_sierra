#include <iostream>
#include "CircularLinkedList.h"
using namespace std;

int main(){
    cout << "Hello world!" << endl;
    CircularLinkedList cll;
    Temperature t(23, 'C');
    cll.InsertHead(t);
    cout << "2nd one" << endl;
    t = Temperature(68, 'F');
    cll.InsertHead(t);
    t = Temperature(70, 'F');
    cll.InsertHead(t);
    t = Temperature(80, 'F');
    cll.InsertHead(t);
    cout << cll << endl;
    return 0;
}
