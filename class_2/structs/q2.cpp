/*
Write sample code to assign values into a Person’s variable, and then print the data out
*/

#include <iostream>
using namespace std;

int main(void){
  struct Height {
   int feet;
   int inches;
  };

  struct Person {
  string name;
  string birthday;
  string address;
  Height height;
  };

  Person person[10];

  person[0].name = "noah";
  person[0].birthday = "090823";
  person[0].address = "28226 kane ct.";
  person[0].height.feet = 5;
  person[0].height.inches = 4;
  return 0;
}
