/*
Create a variable that can store 25 people’s data using the Person data type from question
1:Modify the Person’s structure to allow it to store also the person’s birthday, and address.
*/

#include <iostream>
using namespace std;

int main(void){
  struct Height {
   int feet;
   int inches;
  };

  struct Person {
  string name;
  string birthday;
  string address;
  Height height;
  };

  Person person[25];

  person[0].name = "noah";
  person[0].birthday = "090823";
  person[0].address = "28226 kane ct.";
  person[0].height.feet = 5;
  person[0].height.inches = 4;
  return 0;
}
