/*
Write sample code to assign values into the variable declared in question 3, and then print
the data out.
*/

#include <iostream>
using namespace std;

int main(void){
  struct Height {
   int feet;
   int inches;
  };

  struct Person {
  string name;
  string birthday;
  string address;
  Height height;
  };

  Person person[25];

  for (int i = 0; i < 25; i++){
    person[i].name = "noah";
    person[i].birthday = "090823";
    person[i].address = "28226 kane ct.";
    person[i].height.feet = 5;
    person[i].height.inches = 4;
  }
  for (int i = 0; i < 25; i++){
    cout << person[i].name << endl;
    cout << person[i].birthday << endl;
    cout << person[i].address << endl;
    cout << person[i].height.feet << endl;
    cout << person[i].height.inches << endl;
  }
  return 0;
}
