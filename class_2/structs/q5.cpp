/*
 I operate 25 weather stations around the world.
I want to store the temperatures obtained from these weather stations on a daily basis.
I want to keep a history of these daily temperature readings for 10 days for each weather
station.
Design a data structure for storing these data. Data to store:
 Name of station
 Location of station: longitude and latitude
 Temperature: degree and scale
Write a program to implement this data object. Assign sample data for one station.
*/

#include <iostream>
using namespace std;

int main(void){
  struct Temp {
    int degree;
    char scale;
  };
  struct Location {
    double logitude;
    double latitude;
  };
  struct WeatherStation {
    Temp temp[10];
    string name;
    Location location;
  };

  WeatherStation weatherStation[25];

  weatherStation[0].temp[0].degree = 20;
  weatherStation[0].temp[0].scale = 'c';
  weatherStation[0].name = "some name";
  weatherStation[0].location.logitude = 15.2225;
  weatherStation[0].location.latitude = 15.32;


  cout << weatherStation[0].temp[0].degree;
  cout << weatherStation[0].temp[0].scale << endl;
  cout << weatherStation[0].name << endl;
  cout << weatherStation[0].location.logitude << endl;
  cout << weatherStation[0].location.latitude << endl;


  return 0;
}
