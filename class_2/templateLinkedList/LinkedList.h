#ifndef __LINKEDLISTTEMPLATE__
#define __LINKEDLISTTEMPLATE__
#include <iostream>
using namespace std;
///////////////////////////////////////////////////////////////////
// class template declaration for a ListNode
template <class T>
struct ListNode {
  T data; // the data field is of type T whatever type T is passed in
  ListNode* next;
};
///////////////////////////////////////////////////////////////////
// class template declaration for a LinkedList
template <class T>
class LinkedList {
  private:
    ListNode<T>* Head;
    ListNode<T>* Tail; // for inserting node at the end of the list
  public:
     LinkedList();
     void InsertNode(T d);
     void InsertInOrder(T d);
     ListNode<T>* SearchNode(T d);
     ListNode<T>* SearchNode(T d, ListNode<T>*& pptr);
     bool ChangeNode(T oldD, T newD);
     bool DeleteNode(T d);
     // there are two different methods to overload a friend operator
     // this is method 1 to overload the friend operator <<
     template <class U>
       friend ostream& operator<<(ostream& out, const LinkedList<U>& ll);
     // this is method 2 to overload the friend operator >>
     // for this method the definition must be done inline here in the declaration
     // this function inserts a new node at the end of the list
     friend istream& operator>>(istream& in, LinkedList& ll) {
        ListNode<T>* ptr = new(ListNode<T>);
         in >> ptr->data;
          ptr->next = NULL;
           if (ll.Tail == NULL) { // empty list
              ll.Head = ptr;
               ll.Tail = ptr;
                } else { // insert at end of list
                   ll.Tail->next = ptr;
                    ll.Tail = ptr;
                     }
            return in;
             }
};
///////////////////////////////////////////////////////////////////
// class template definition. Must be in this .h file
//
// constructor initialize list to null
template <class T>
LinkedList<T>::LinkedList() {
   Head = NULL;
    Tail = NULL;
}
// insert new node with the given data at the head of the list
template <class T>
void LinkedList<T>::InsertNode(T d) {
  ListNode<T>* ptr = new(ListNode<T>);
   ptr->data = d;
    ptr->next = Head;
     Head = ptr;
     if (Tail == NULL) { // empty list
        Tail = ptr;
         }
}
// insert new node with the given data in the list that keeps the list sorted
template <class T>
void LinkedList<T>::InsertInOrder(T d) {
  ListNode<T>* ptr = Head;
  ListNode<T>* pptr = NULL; // previous pointer
  //me confused, question not that well written, like should i insert at Head, before the Head, in middle of list, at tail, how would the list be unsorted?

}
// search for node in list
// returns pointer to node if found
// else returns NULL
template <class T>
ListNode<T>* LinkedList<T>::SearchNode(T d) {
  ListNode<T>* ptr;
   ptr = Head;
   while (ptr != NULL) { // traverse list
      if (ptr->data == d) { // found node
         return ptr; // return pointer to found node
          }
       ptr = ptr->next; // go to next node
        }
   return NULL;
}
// search for node in list
// returns pointer to node if found
// else returns NULL
// also returns the previous pointer in argument list
template <class T>
ListNode<T>* LinkedList<T>::SearchNode(T d, ListNode<T>*& pptr) {
  ListNode<T>* ptr = Head;
  pptr = NULL;
  while (ptr != NULL) { // traverse list
     if (ptr->data == d) { // found node
        return ptr; // return pointer to found node
         }
      pptr = ptr; // update previous pointer
       ptr = ptr->next; // go to next node
        }
  return NULL;
}
template <class T>
bool LinkedList<T>::ChangeNode(T oldD, T newD) {
  ListNode<T>* ptr = SearchNode(oldD); // search for node
  if (ptr) { // found node to change
     ptr->data = newD;
      return true;
       } else {
          return false;
           }
}
// delete node with the given data from the list
template <class T>
bool LinkedList<T>::DeleteNode(T d) {
  ListNode<T>* ptr;
  ListNode<T>* pptr; // previous pointer
   ptr = SearchNode(d, pptr); // search for node
   if (ptr != NULL) { // found node to delete
      if (ptr == Head) { // need to treat the first node differently
         Head = Head->next; // delete the head node
          } else { // delete a non-head node
             pptr->next = ptr->next;
              }
       delete ptr; // release memory
        return true;
         }
   else {
     return false;
      }
}
template <class U>
ostream& operator<<(ostream& out, const LinkedList<U>& ll) {
   ListNode<U>* ptr;
    ptr = ll.Head;
     while (ptr != NULL) {
        out << ptr->data << ", ";
         ptr = ptr->next;
          }
      cout << endl;
      return out;
       }
#endif

