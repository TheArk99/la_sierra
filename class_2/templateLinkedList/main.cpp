#include <iostream>
#include "LinkedList.h"
#include "temperature/temperatureObj.h"
using namespace std;
int main() {
  // linked list of integers
  LinkedList<int> L1;
  L1.InsertNode(6);
  L1.InsertNode(5);
  L1.InsertNode(4);
  L1.InsertNode(3);
  L1.InsertNode(2);
  cout << "Enter an int? ";
  cin >> L1;
  cout << "Here's the linked list after inserting 6 integers..." << endl;
  cout << L1;
  L1.DeleteNode(4);
  L1.DeleteNode(1);
  cout << "Here's the linked list after deleting nodes 4 and 1..." << endl;
  cout << L1;
  // linked list of characters
  LinkedList<char> L2;
  L2.InsertNode('U');
  L2.InsertNode('S');
  L2.InsertNode('L');
  L2.InsertNode('@');
  L2.InsertNode('S');
  L2.InsertNode('C');
  cout << "Here's the linked list after inserting 6 characters..." << endl;
  cout << L2;
  L2.DeleteNode('C');
  cout << "Here's the linked list after deleting node C..." << endl;
  L2.ChangeNode('@', 'A');
  cout << "Here's the linked list after changing node @ with A..." << endl;
  cout << L2;
  // linked list of Temperatures
  LinkedList<Temperature> L3;
  L3.InsertNode(Temperature(78,'F'));
  L3.InsertNode(Temperature(21,'C'));
  cout << "Here's the linked list of Temperatures..." << endl;
  cout << L3;

  //Temperatures q1:
  LinkedList<Temperature> L4;
  L4.InsertNode(Temperature(5, 'c'));
  L4.InsertNode(Temperature(2, 'c'));
  L4.InsertNode(Temperature(8, 'c'));
  L4.InsertNode(Temperature(7, 'c'));
  L4.InsertNode(Temperature(0, 'c'));

  L4.ChangeNode(Temperature(5, 'c'), Temperature(3, 'c'));
  L4.DeleteNode(Temperature(0, 'c'));

  cout << "Enter Temperature: ";
  cin >> L4;

  cout << "Here's the linked list of Temperatures..." << endl;
  cout << L4;
  return 0;
}
