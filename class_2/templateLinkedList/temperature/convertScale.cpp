#include "convertScale.h"
#include <iostream>
using namespace std;

double scaleKToC(double k){
  double c = (double)k - 273.15;
  return c;
}
double scaleFToC(double f){
  double c = (double)((f - 32) * 5)/9;
  return c;
}

void convertTwoDegrees(const char& scale1, const char& scale2, double& degree1, double& degree2){
//void convertTwoDegrees(double* degree1, double* degree2){ //C like way that i am more used to
  if (scale1 == 'f'){
    degree1 = scaleFToC(degree1);
    //*degree1 = scaleFToC(degree1);
  }else if (scale1 == 'k'){
    degree1 = scaleKToC(degree1);
    //*degree1 = scaleKToC(degree1);
  }

  if(scale2 == 'f'){
    degree2 = scaleFToC(degree2);
    //*degree2 = scaleFToC(degree2);
  }else if (scale2 == 'k'){
    degree2 = scaleKToC(degree2);
    //*degree2 = scaleKToC(degree2);
  }
}
