#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include "temperature/temperatureObj.h"
#include "list.h"
using namespace std;

//destructor
LinkedList::~LinkedList(){
    Node* current = Head;
    while (current != nullptr) {
      Node* temp = current;
      current = current->next;
      delete(temp);
    }
}

void LinkedList::insertFront(int data) {
    Node* newNode = new Node(data);
    if (Head == nullptr) {
        Head = newNode;
        Tail = newNode;
    } else {
        newNode->next = Head;
        Head->prev = newNode;
        Head = newNode;
    }
}

// Function to insert at the end
void LinkedList::insertEnd(int data) {
    Node* newNode = new Node(data);
    if (Tail == nullptr) {
        Head = newNode;
        Tail = newNode;
    } else {
        Tail->next = newNode;
        newNode->prev = Tail;
        Tail = newNode;
    }
}

// Function to print the list
void LinkedList::display_start() {
    Node* current = Head;
    while (current != nullptr) {
        std::cout << current->data << " ";
        current = current->next;
    }
    std::cout << std::endl;
}

void LinkedList::display_end() {
    Node* current = Tail;
    while (current != nullptr) {
        std::cout << current->data << " ";
        current = current->prev;
    }
    std::cout << std::endl;
}

//function to search the list
Node *LinkedList::searchList(const int& num){
  Node* temp = Head;
  while (temp != nullptr){
    if (temp->data == num){
      return temp;
    }
    temp = temp->next;
  }
  return nullptr;
}

//function to del a node
void LinkedList::rmNode(Node **toDel){
  if (Head == *toDel){
    Head = (*toDel)->next;
    if (Head != nullptr)
      Head->prev = nullptr;
    delete *toDel;
    return;
  }else{
    (*toDel)->prev->next = (*toDel)->next;
    if ((*toDel)->next != nullptr)
      (*toDel)->next->prev = (*toDel)->prev;
    delete *toDel;
  }
}

void LinkedList::toDoWithNode(int& num){
  Node *findNode = searchList(num);
  if (findNode != nullptr){
    cout << "found: " << findNode->data << endl;
    cout << "would you like to replace num? [y/n] ";
    char answer;
    cin >> answer;

    if (answer == 'Y' || answer == 'y'){
      cout << "enter new num: ";
      int newNum;
      cin >> newNum;
      findNode->data = newNum;
    }else{

      cout << "would you like to delete num? [y/n] ";
      cin >> answer;

      if (answer == 'Y' || answer == 'y'){
        cout << "deleting..." << endl;
        rmNode(&findNode);
      }

    }

  }

}
