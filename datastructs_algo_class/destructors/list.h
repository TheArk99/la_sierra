#ifndef __LIST__
#define __LIST__

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include "temperature/temperatureObj.h"
using namespace std;

// Define a Node structure
struct Node {
  int data;
  Node* next;
  Node* prev;

  // Constructor
  Node(int d) : data(d), next(nullptr), prev(nullptr) {}
};

// Define LinkedList class
class LinkedList {
private:
  Node* Head;
  Node* Tail;

public:
  // Constructor
  LinkedList() : Head(nullptr), Tail(nullptr) {}

  // destructor
  ~LinkedList();

  // Function to insert at the front
  void insertFront(int data);
  // Function to insert at the end
  void insertEnd(int data);
  // Function to print the list
  void display_start();
  void display_end();
  //function to search the list
  Node *searchList(const int& num);
  //function to del a node
  void rmNode(Node **toDel);
  void toDoWithNode(int& num);

};

#endif

