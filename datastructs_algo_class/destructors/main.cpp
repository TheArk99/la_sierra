#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstdlib>
#include "temperature/temperatureObj.h"
#include "list.h"
using namespace std;


int main() {
  LinkedList list;

  // Insert elements at the front
  list.insertFront(3);
  list.insertFront(2);
  list.insertFront(1);

  // Insert elements at the end
  list.insertEnd(4);
  list.insertEnd(5);

  int numToSearch;
  cout << "Enter num to find in list: ";
  cin >> numToSearch;
  list.toDoWithNode(numToSearch);

  cout << "List from start to end: ";
  list.display_start();

  cout << "List from end to start: ";
  list.display_end();

  return 0;
}
