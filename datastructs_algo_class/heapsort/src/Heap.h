/********************************************************************
 * Heapsort
 * Reference: Corman, Leiserson, & Rivest, "Introduction to Algorithms" pp. 140-147
 *
 * May 2001 Enoch Hwang
 */
#include <iostream>
#include <queue>
#include <vector>
#include <functional>
#include <algorithm>
using namespace std;

#ifndef __HEAP__
#define __HEAP__

#define HEAP_SIZE 10
//const int HEAP_SIZE = 10;

class heap{
  public:
    heap();
    void heapSort();
    friend ostream & operator<<(ostream & out, const heap & theHeap);
    friend istream & operator>>(istream & in, heap & theHeap);

  private:
    int heapArr[HEAP_SIZE + 1];
    int heap_size;
    int legnth;

    void buildHeap();
    void heapify(int i);
    void heapify(int arr[], int i);
    int left(int i);
    int right(int i);
    int parent(int i);

};
#endif

