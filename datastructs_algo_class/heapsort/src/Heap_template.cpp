/********************************************************************
 * Heapsort
 * Reference: Corman, Leiserson, & Rivest, "Introduction to Algorithms" pp. 140-147
 *
 * May 2001 Enoch Hwang
 */


//notes for accessing Heap as an array
/*
to get left child you do i * 2
to get right child you do i * 2 + 1
to get parent of a child you do i / 2
*/

#include "Heap.h"

heap::heap(){
  heapArr[1]=4;
  heapArr[2]=1;
  heapArr[3]=3;
  heapArr[4]=2;
  heapArr[5]=16;
  heapArr[6]=9;
  heapArr[7]=10;
  heapArr[8]=14;
  heapArr[9]=8;
  heapArr[10]=7;
}


void heap::heapSort(){
  buildHeap();

//non queue version
  for (int i = HEAP_SIZE; i >= 2; i--){
    int temp = heapArr[1];
    heapArr[1] = heapArr[i];
    heapArr[i] = temp;
    legnth = legnth - 1;
    heapify(1);
  }


/*
//stl queue version
  priority_queue<int, vector<int>, greater<int>> heapQueue; // least is first
//  priority_queue<int> heapQueue; // max is first

  //for (int i = HEAP_SIZE; i >= 1; i--){ // if using max first queue
  for (int i = 1; i <= HEAP_SIZE; i++){ // if using least first queue
    heapQueue.push(heapArr[i]);
  }

  //for (int i = HEAP_SIZE; i >= 1; i--){ // if using max first queue
  for (int i = 1; i <= HEAP_SIZE; i++){ // if using least first queue
    heapArr[i] = heapQueue.top();
    heapQueue.pop();
  }
*/
}


void heap::buildHeap(){
  legnth = HEAP_SIZE;
  for (int i = HEAP_SIZE / 2; i >= 1; i--){
    heapify(i);
  }
}


void heap::heapify(int i){
  int leftChildIndex = left(i);
  int rightChildIndex = right(i);
  int largest;
  if (leftChildIndex <= legnth && heapArr[leftChildIndex] > heapArr[i]){
    largest = leftChildIndex;
  }else{
    largest = i;
  }

  if (rightChildIndex <= legnth && heapArr[rightChildIndex] > heapArr[largest]){
    largest = rightChildIndex;
  }

  if (largest != i){
    int temp = heapArr[i];
    heapArr[i] = heapArr[largest];
    heapArr[largest] = temp;
    heapify(largest);
  }

}

void heap::heapify(int arr[], int i){
  int leftChildIndex = left(i);
  int rightChildIndex = right(i);
  int largest;
  if (leftChildIndex <= legnth && arr[leftChildIndex] > arr[i]){
    largest = leftChildIndex;
  }else{
    largest = i;
  }

  if (rightChildIndex <= legnth && arr[rightChildIndex] > arr[largest]){
    largest = rightChildIndex;
  }

  if (largest != i){
    int temp = arr[i];
    arr[i] = arr[largest];
    arr[largest] = temp;
    heapify(largest);
  }

}

int heap::left(int i){
  return i * 2;
}

int heap::right(int i){
  return i * 2 + 1;
}

int heap::parent(int i){
  return i / 2;
}

ostream & operator<<(ostream & out, const heap & theHeap){
  out << "root: " << theHeap.heapArr[1] << endl;
  for(int i = 1; i<= HEAP_SIZE; i++)
    out << theHeap.heapArr[i] << ' ';
  return out;
}

istream & operator>>(istream & in, heap & theHeap){
  for(int i = 1; i <= HEAP_SIZE; i++){
    cout << i << "? ";
    in >> theHeap.heapArr[i];
  }
  return in;
}
