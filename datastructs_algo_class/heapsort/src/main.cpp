#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <stdio.h>
#include <ctime>
#include <list>
#include <map>
#include <vector>
#include "Heap.h"
using namespace std;

/********************************************************************
 * Heapsort
 * Reference: Corman, Leiserson, & Rivest, "Introduction to Algorithms" pp. 140-147
 *
 * May 2001 Enoch Hwang
 */


int main(){

  heap h;
  cout << "Original numbers..." << endl;
  cout << h << endl;

  // cout << "Enter " << HEAP_SIZE << " numbers? " << endl;
  // cin >> h;
  // cout << "New numbers..." << endl;
  // cout << h << endl;

  h.heapSort();

  cout << "Sorted numbers..." << endl;
  cout << h << endl;
  return 0;
}
