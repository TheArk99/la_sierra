#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <stdio.h>
#include <ctime>
using namespace std;

int main(int argc, char** argv){
  srand(time(0));
  int bytes;
 // int *var_to_allocate = (int*)malloc(sizeof(int) * 1);
  int *var_to_allocate = nullptr;
  if (argc >= 2){
    string temp_arg = argv[2];
    bytes = stoi(temp_arg);
  }else{
    bytes = 100000000;
  }

  int i = 0;
  while (true){
    try {
    var_to_allocate = new int[bytes]();
//    var_to_allocate = (int*)realloc(var_to_allocate, sizeof(int) * (i+1));
//    var_to_allocate[i] = rand();
    } catch (const bad_alloc& e){
      cerr << "Memory allocation failed: " << e.what() << endl;
      return 1;
    }
    cout << "times run: " << i << endl;
    i++;
  }
}
