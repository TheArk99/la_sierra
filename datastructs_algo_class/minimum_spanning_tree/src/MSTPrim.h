/* Minimum Spanning Tree (MST) using Prim's algorithm
 * A weighted graph using an ajacency matrix
 */
#include <bits/stdc++.h> // for INT_MAX
#include <iostream>
using namespace std;

#ifndef __MSTPRIM__
#define __MSTPRIM__

#define CLEAR "\033[2J\033[1;1H"
#define CYAN "\033[0;36m"
#define WHITE "\033[0;37m"

class MSTPrim {
public:
  MSTPrim();
  void Prim();
  void PrintGraph();
  void PrintSolution();

private:
  static const int SIZE = 9; // number of vertices in graph
  // int aMatrix[SIZE][SIZE] = {{0}};

  int aMatrix[SIZE][SIZE] = {
    // a  b  c  d  e  f  g  h  i
      {0, 4, 0, 0, 0, 0, 0, 8, 0},   // a
      {4, 0, 8, 0, 0, 0, 0, 11, 0},  // b
      {0, 8, 0, 7, 0, 4, 0, 0, 2},   // c
      {0, 0, 7, 0, 9, 14, 0, 0, 0},  // d
      {0, 0, 0, 9, 0, 10, 0, 0, 0},  // e
      {0, 0, 4, 14, 10, 0, 2, 0, 0}, // f
      {0, 0, 0, 0, 0, 2, 0, 1, 6},   // g
      {8, 11, 0, 0, 0, 0, 1, 0, 7},  // h
      {0, 0, 2, 0, 0, 0, 6, 7, 0}};  // i

  // Array to store constructed MST
  int parent[SIZE];
  // Key values used to pick minimum weight edge in cut
  int key[SIZE];
  bool set[SIZE]; // set[i] will be true if vertex i is in the set, i.e., included in the MST

  int minDistance();  // find min distance crossing the cut
  char toChar(int i); // utility function to convert vertex number to vertex letter

};
#endif
