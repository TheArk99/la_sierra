#include "MSTPrim.h"

void MSTPrim::PrintGraph() {
  cout << CLEAR << CYAN << "\tGraph" << endl;
  for (int j = 0; j < SIZE; j++) {
    cout << "\t" << toChar(j);
  }
  cout << WHITE << endl;

  for (int i = 0; i < SIZE; i++) {
    cout << CYAN << toChar(i) << WHITE;
    for (int j = 0; j < SIZE; j++) {
      cout << "\t" << aMatrix[i][j];
    }
    cout << endl;
  }
  cout << endl;
}

void MSTPrim::PrintSolution() {
  int total = 0;
  cout << CYAN << "Minimum Spanning Tree" << endl;
  cout << CYAN << "Edge\t  Weight\n" << WHITE;
  for (int i = 1; i < SIZE; i++) {
    cout << toChar(parent[i]) << " - " << toChar(i) << "\t\t" << aMatrix[i][parent[i]] << endl;
    total += aMatrix[i][parent[i]];
  }
  cout << endl << "Total weight: " << total << endl;
}

MSTPrim::MSTPrim() {
  // Initialize all keys to infinity and nothing in the set
  for (int i = 0; i < SIZE; i++) {
    key[i] = INT32_MAX;
    set[i] = false;
    parent[i] = -1;
  }
}

//FILL IN!!!!!
// find min distance of a node that is not already in the set
// and return the node for that min distance
int MSTPrim::minDistance() {
  // Initialize min SIZEalue
  int min = INT32_MAX;
//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wmaybe-uninitialized" // for ignoring min_index being uninit
  int min_index = -1; // index of where the min SIZEalue is at
  for (int i = 0; i < SIZE; i++) {
    if (!set[i] && key[i] < min) {
      min = key[i];
      min_index = i;
    }
  }
  return min_index;
//#pragma GCC diagnostic pop // stop ignoring uninit
}

// find minimum spanning tree
void MSTPrim::Prim() {
  for (int i = 0; i < SIZE; i++) {
    key[i] = INT32_MAX;
    set[i] = false;
  }

  key[0] = 0;
  parent[0] = -1;

//  for (int i = 0; i < SIZE - 1; i++) {
  for (int i = SIZE; i > 0; i--) { // more how the book had it
    int u = minDistance();
    set[u] = true;

    for (int j = 0; j < SIZE; j++){
      if (aMatrix[u][j] && !set[j] && aMatrix[u][j] < key[j]){
        parent[j] = u;
        key[j] = aMatrix[u][j];
      }
    }
  }

}

// utility function to convert vertex number to vertex letter
char MSTPrim::toChar(int i) {
  return 'a'+i;
}
