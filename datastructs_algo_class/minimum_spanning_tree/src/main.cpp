#include "MSTPrim.h"

int main() {
  MSTPrim mst;
  mst.PrintGraph();
  mst.Prim();
  mst.PrintSolution();

  return 0;
}