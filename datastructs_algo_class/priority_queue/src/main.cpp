#include <iostream>
#include "priorityQueue.h"
using namespace std;

int main() {
  PriorityQueue pq;
  pq.Push(5);
  pq.Push(10);
  pq.Push(7);
  pq.Push(6);
  pq.Push(3);
  pq.Push(1);
  pq.Push(9);

  cout << pq << endl;

  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
  cout << "Top is: " << pq.Top() << endl;
  pq.Pop();
}
