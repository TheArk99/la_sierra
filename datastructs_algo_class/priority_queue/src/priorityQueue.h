#include <iostream>
using namespace std;

#ifndef __PRIORITYQUEUE__
#define __PRIORITYQUEUE__

#define ARRAYSIZE 10

class PriorityQueue{
  public:
    PriorityQueue();
    int Top();
    bool Push(int v);
    void Pop();

    friend ostream & operator<<(ostream & out, const PriorityQueue & theQueue);
    friend istream & operator>>(istream & in, PriorityQueue & theQueue);

  private:
    int A[ARRAYSIZE+1];    // A[0] is not used
    int Qend; // end of queue
    int Qstart; // begging of queue

    void BuildHeap();
    void Heapify(int i, int heap_size);
    int Left(int i);
    int Right(int i);
    bool Compare(int n1, int n2);
};
#endif

