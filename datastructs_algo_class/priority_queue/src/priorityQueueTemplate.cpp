#include "priorityQueue.h"

PriorityQueue::PriorityQueue() {
  Qend = 0;
  Qstart = 1;
}

int PriorityQueue::Top() {
  if (Qstart <= Qend){
    BuildHeap();
    return A[1];  // A[0] is not used
  }else{
    return -1;
  }
}

bool PriorityQueue::Push(int v) {
  if (Qend < ARRAYSIZE) {
    Qend++;
    A[Qend] = v;
    BuildHeap();
    return true;
  }
  return false;
}

void PriorityQueue::Pop() {
  if (Qstart <= Qend){
    A[1] = A[Qend];
    Qend--;
    Heapify(1, Qend);
  }
}

void PriorityQueue::Heapify(int i, int heap_size) {
  int l = Left(i);
  int r = Right(i);
  int largest = i;
  if (l <= heap_size && Compare(A[l], A[largest])) {
      largest = l;
  } else {
      largest = i;
  }
  if (r <= heap_size && Compare(A[r], A[largest])) {
      largest = r;
  }
  if (largest != i) {
    int t = A[i];
    A[i] = A[largest];
    A[largest] = t;
    Heapify(largest, heap_size);
  }
}

void PriorityQueue::BuildHeap(){
  for(int i = Qend / 2; i >= 1; i--){
    Heapify(i, Qend);
  }
}

int PriorityQueue::Left(int i) {
  return i*2;
}

int PriorityQueue::Right(int i) {
  return i*2+1;
}

bool PriorityQueue::Compare(int n1, int n2) {
  return n1 < n2;
}


ostream & operator<<(ostream & out, const PriorityQueue & theQueue){
  for(int i = theQueue.Qstart; i<=theQueue.Qend; i++)
    out << theQueue.A[i] << ' ';
  return out;
}

istream & operator>>(istream & in, PriorityQueue & theQueue){
  for(int i = 1; i<=theQueue.Qend; i++){
    cout << i << "? ";
    in >> theQueue.A[i];
  }
  return in;
}
