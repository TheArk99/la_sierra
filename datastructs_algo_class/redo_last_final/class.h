#ifndef __CLASS__
#define __CLASS__

#include <iostream>
#include <string>
#include <cstring>
using namespace std;



class City{
  private:
    string city_name;
  public:
    City() : city_name("") {};
    City(string s) : city_name(s) {};
//    bool operator==(const City& city);
    bool operator!=(const City& city);
    bool operator!=(const string c);
    friend ostream& operator<<(ostream& out, const City& city);
};

struct linkedList {
  City data;
  linkedList* next;
  linkedList* prev;

  // Constructor
  linkedList() : data(City()), next(nullptr), prev(nullptr) {}
  linkedList(City d) : data(d), next(nullptr), prev(nullptr) {}
};

linkedList *insertNode(City data);

extern linkedList *Head;
extern linkedList *Tail;

#endif

